using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using System;

//Originally written by Tim Harrison  on 13/8/22
//Edited by Tim Harrison on 20/8/22 - now takes string in update tile and checks if dictionary contains that string as a key

public class TileManager : MonoBehaviour
{
    private Tilemap[] maps;
    private Dictionary<string, Tilemap> tileMaps = new Dictionary<string, Tilemap>();
    private static TileManager tileManagerInstance;

    

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;

        DontDestroyOnLoad(transform.gameObject);

        if (tileManagerInstance == null)
        {
            tileManagerInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //Refinds tilemaps in each scene
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        maps = FindObjectsOfType<Tilemap>();
        tileMaps.Clear();

        foreach (Tilemap t in maps)
        {
            tileMaps.Add(t.name, t);
        }
    }

    //Cleans up listeners
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    //Changes a tile in the tile map at the given position to the given tile in the given map
    public void UpdateTile(Tile tile, string map, Vector3Int position)
    {
        if (tileMaps.ContainsKey(map))
        {
            tileMaps[map].SetTile(position, tile);
        }
        else
        {
            Debug.Log($"ERROR: TILEMAP {map} DOES NOT EXIST IN THIS SCENE");
        }
    }
}
