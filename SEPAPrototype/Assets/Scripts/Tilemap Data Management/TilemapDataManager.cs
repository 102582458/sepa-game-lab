using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

//Edited by Nicholas Mueller on 25/9/22 - Updated the script to take in a list of tilemaps  
//Edited by Tim Harrison on 28/9/22 - now only checks tiles on the active tilemap

public class TilemapDataManager : MonoBehaviour
{
    [SerializeField] private List<Tilemap> maps;
    [SerializeField] private List<TileData> tileDatas;
    private Dictionary<TileBase, TileData> dataFromTiles;

    private Vector3Int gridPosition;
    private TileBase tile;
    private bool tileIsIceCheck = false, speedMultiplierCheck = false;

    private Tilemap activeMap;

    private void Awake()
    {
        dataFromTiles = new Dictionary<TileBase, TileData>();

        foreach (var tileData in tileDatas)
        {
            foreach (var tile in tileData.tiles)
            {
                dataFromTiles.Add(tile, tileData);
            }
        }

        activeMap = maps[0];
    }

    //Sets the active tile map
    public void SetActiveMap(int i)
    {
        activeMap = maps[i];
    }

    public bool IsIce(Vector2 worldPosition)
    {
        bool tileIsIce = false;

        if (tileIsIceCheck == false)
        {
            gridPosition = activeMap.WorldToCell(worldPosition);
            if (gridPosition != null)
            {
                tile = activeMap.GetTile(gridPosition);
            }
            else
            {
                tile = null;
            }

            if (tile != null)
            {
                if (dataFromTiles.ContainsKey(tile))
                {
                    tileIsIceCheck = true;
                    tileIsIce = dataFromTiles[tile].isIce;
                }
            }
        }

        tileIsIceCheck = false;
        return tileIsIce;
    }

    public float GetSpeedMultiplier(Vector2 worldPosition)
    {
        float speedMultiplier = 1f;

        if (speedMultiplierCheck == false)
        {
            gridPosition = activeMap.WorldToCell(worldPosition);
            if (gridPosition != null)
            {
                tile = activeMap.GetTile(gridPosition);
            }
            else
            {
                tile = null;
            }

            if (tile != null)
            {
                if (dataFromTiles.ContainsKey(tile))
                {
                    speedMultiplierCheck = true;
                    speedMultiplier = dataFromTiles[tile].speedMultiplier;
                }
            }
        }

        speedMultiplierCheck = false;
        return speedMultiplier;
    }
}
