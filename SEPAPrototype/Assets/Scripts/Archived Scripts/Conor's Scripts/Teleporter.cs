using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public KeyCode teleport;
    public GameObject player1;
    public GameObject player2;
    private Vector2 tempPosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(teleport))
        {
            tempPosition = player1.transform.position;
            player1.transform.position = player2.transform.position;
            player2.transform.position = tempPosition;
        }
    }
}
