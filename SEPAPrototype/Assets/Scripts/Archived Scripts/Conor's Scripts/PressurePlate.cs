using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    
    public GameObject door;
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        door.SetActive(false);
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        door.SetActive(true);
    }
   
}
