using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportWarp : MonoBehaviour
{


    public bool respawnBothPlayers;
    public GameObject player1;
    public GameObject player2;
    public GameObject player1Warp;
    public GameObject player2Warp;

    private BoxCollider2D teleporterBC;
    private BoxCollider2D player1BC;
    private BoxCollider2D player2BC;
    

    // Start is called before the first frame update
    void Start()
    {
        player1BC = player1.GetComponent<BoxCollider2D>();
        player2BC = player2.GetComponent<BoxCollider2D>();
        teleporterBC = this.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(respawnBothPlayers == true)
        {
            if (player1BC.IsTouching(teleporterBC) || player2BC.IsTouching(teleporterBC))
            {
                player1.transform.position = player1Warp.transform.position;
                player2.transform.position = player2Warp.transform.position;
            }
        }
        else
        {
            if (player1BC.IsTouching(teleporterBC))
            {
                player1.transform.position = player1Warp.transform.position;
            }
            if (player2BC.IsTouching(teleporterBC))
            {
                
                player2.transform.position = player2Warp.transform.position;
            }
        }
        

    }
}
