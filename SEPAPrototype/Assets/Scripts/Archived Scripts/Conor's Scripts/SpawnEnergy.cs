using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnergy : MonoBehaviour
{
    public GameObject energy;
    public KeyCode button;
    public Transform spawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(button))
        {
            Instantiate(energy, spawnPoint.position, spawnPoint.rotation);
            
        }
        
    }
}
