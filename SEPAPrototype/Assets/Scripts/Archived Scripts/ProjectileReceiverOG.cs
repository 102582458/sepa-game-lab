using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileReceiverOG : MonoBehaviour
{
    [SerializeField] private List<GameObject> SetActive;
    [SerializeField] private List<GameObject> SetInactive;
    [SerializeField] private GameObject[] pairedButton;
    [SerializeField] private GameObject soundManagerObj;
    private SoundManager soundManager;
    private string laserTag = "Laser";

    //public GameObject SetActiveWall;
    //public GameObject SetInactiveWall;

    //[SerializeField] private GameObject player1;
    //[SerializeField] private GameObject player2;

    //public GameObject pairedButton;

    public bool isDependant;

    //private BoxCollider2D buttonBC;
    //private BoxCollider2D player1BC;
    //private BoxCollider2D player2BC;

    //the amount of time the reciver counts as active
    public float activeTime = 3.0f;
    //current time till deactive
    private float timer = 0f;
    //sets the sprite
    public SpriteRenderer spriteRenderer;
    public Sprite deactiveSprite;
    public Sprite activeSprite;
    public bool bothActive = false;

    private ProjectileReceiverOG otherButtonS;

    private bool colliding;
    

    // Start is called before the first frame update
    void Start()
    {
        soundManager = soundManagerObj.GetComponent<SoundManager>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (pairedButton.Length == 1)
        {
            otherButtonS = pairedButton[0].GetComponent<ProjectileReceiverOG>();
        }

        colliding = false;
    }

    // Update is called once per frame
    void Update()
    {

        timer -= Time.deltaTime;

        if (timer <= 0.0f && bothActive == false)
        {
            colliding = false;
            spriteRenderer.sprite = deactiveSprite;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        soundManager.PlaySound("buttonPress");
        if(collision.gameObject.tag == laserTag)
        {
            if (!isDependant)
            {
                spriteRenderer.sprite = activeSprite;
                bothActive = true;
                SetState();
                //SetInactiveWall.SetActive(false);
            }
            else
            {
                colliding = true;
                spriteRenderer.sprite = activeSprite;
                timer = activeTime;
                if (otherButtonS.colliding)
                {
                    bothActive = true;

                    SetState();
                    //SetInactiveWall.SetActive(false);
                }
            }
        }
        
    }

    

    private void SetState()
    {
        foreach (GameObject g in SetInactive)
        {
            if (g.name == "LaserDoor")
            {
                soundManager.PlaySound("gateOpening");
            }
            g.SetActive(false);
        }
        foreach (GameObject g in pairedButton)
        {
            g.GetComponent<ProjectileReceiverOG>().bothActive = true;
        }

        foreach (GameObject g in SetActive)
        {
            g.SetActive(true);
        }
    }
}
