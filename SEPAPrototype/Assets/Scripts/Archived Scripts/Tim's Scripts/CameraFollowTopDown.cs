using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTopDown : MonoBehaviour
{
    private float distanceX;
    private float distanceY;

    public GameObject player1;
    public GameObject player2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate()
    {
        distanceX = getMidpoint(player1.transform.localPosition.x, player2.transform.localPosition.x);
        distanceY = getMidpoint(player1.transform.localPosition.y, player2.transform.localPosition.y);

        transform.position = new Vector3(distanceX, distanceY, transform.position.z);
    }

    private float getMidpoint(float i, float j)
    {
        return (i + j) / 2;
    }
}