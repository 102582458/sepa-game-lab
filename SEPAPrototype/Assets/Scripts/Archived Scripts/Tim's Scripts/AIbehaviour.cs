using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIbehaviour : MonoBehaviour
{
    private float distanceX;
    private float distanceY;
    private float speed;
    private float range;
    private string currentState;
    private PCmovement enemyPlayerS;
    private Vector2 startingPos;    

    public GameObject enemyPlayer;    
    public string faction;
    

    // Start is called before the first frame update
    void Start()
    {
        speed = 3;
        range = 5;
        startingPos = transform.localPosition;
        enemyPlayerS = enemyPlayer.GetComponent<PCmovement>();
        currentState = "resting";
    }

    // Update is called once per frame
    void Update()
    {
        distanceX = getDifference(enemyPlayer.transform.localPosition.x, transform.localPosition.x);
        distanceY = getDifference(enemyPlayer.transform.localPosition.y, transform.localPosition.y);

        if (currentState == "resting")
        {
            if (distanceX <= range && distanceY <= range && enemyPlayerS.faction != faction)
            {
                currentState = "chasing";
            }
        }
        else if (currentState == "chasing")
        {
            transform.localPosition = Vector2.MoveTowards(transform.localPosition, enemyPlayer.transform.localPosition, speed*Time.deltaTime);
            if (distanceX > range || distanceY > range)
            {
                currentState = "returning";
            }
        }
        else if (currentState == "returning")
        {
            transform.localPosition = Vector2.MoveTowards(transform.localPosition, startingPos, speed*Time.deltaTime);
            if (transform.localPosition.x == startingPos.x && transform.localPosition.y == startingPos.y)
            {
                currentState = "resting";
            }
            else if (distanceX <= range && distanceY <= range && enemyPlayerS.faction != faction)
            {
                currentState = "chasing";
            }
        }
    }

    private float getDifference(float i, float j)
    {
        if (i > j)
        {
            return i - j;
        }
        else
        {
            return j - i;
        }
    }
}
