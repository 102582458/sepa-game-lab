using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGO : MonoBehaviour
{
    private Rigidbody2D goRB;
    private SpriteRenderer goSR;

    // Start is called before the first frame update
    void Start()
    {
        //createGameObjectWall("wall", 0, 0, 01f500);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void createGameObjectWall(string title, float x, float y, UnityEngine.Color color)
    {
        GameObject go = new GameObject(title, typeof(Rigidbody), typeof(BoxCollider), typeof(SpriteRenderer));

        go.transform.localPosition = new Vector2(x, y);
        goSR = go.GetComponent<SpriteRenderer>();
        goSR.color = color;

        goRB = go.GetComponent<Rigidbody2D>();
        //goRB.bodyType = static;

        //GameObject gameobject = new GameObject(title, typeof(Rigidbody, typeof(BoxCollider)))
    }
}
