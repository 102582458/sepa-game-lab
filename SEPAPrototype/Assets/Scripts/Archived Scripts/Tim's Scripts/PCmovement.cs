using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCmovement : MonoBehaviour
{
    [SerializeField] private string characterAxisX;
    [SerializeField] private string characterAxisY;
    public string faction;

    private Rigidbody2D rb;
    private Vector2 playerVelocity;
    [SerializeField] private float speed = 3f;
    private float dirAxesX;
    private float dirAxesY;

    //Used to determine the player facing direction
    private bool playerFacingRight = true;

    // Start is called before the first frame update
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        dirAxesX = Input.GetAxisRaw(characterAxisX);
        dirAxesY = Input.GetAxisRaw(characterAxisY);
        playerVelocity = new Vector2(dirAxesX * speed, dirAxesY * speed);
    }

    private void FixedUpdate()
    {
        rb.velocity = playerVelocity;

        if (dirAxesX < 0 && playerFacingRight)
        {
            FlipPlayer();
        }
        else if (dirAxesX > 0 && !playerFacingRight)
        {
            FlipPlayer();
        }
        /*if (Input.GetKey(up))
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + speed * Time.deltaTime);
        }
        if (Input.GetKey(down))
        {
            transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y - speed * Time.deltaTime);
        }
        if (Input.GetKey(left))
        {
            transform.localPosition = new Vector2(transform.localPosition.x - speed * Time.deltaTime, transform.localPosition.y);
        }
        if (Input.GetKey(right))
        {
            transform.localPosition = new Vector2(transform.localPosition.x + speed * Time.deltaTime, transform.localPosition.y);
        }*/
    }

    private void FlipPlayer()
    {
        playerFacingRight = !playerFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
