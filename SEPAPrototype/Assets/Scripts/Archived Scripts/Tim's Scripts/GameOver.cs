using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public GameObject enemyPlayer;

    private BoxCollider2D enemyPlayerBC;
    private BoxCollider2D buttonBC;

    // Start is called before the first frame update
    void Start()
    {
        enemyPlayerBC = enemyPlayer.GetComponent<BoxCollider2D>();
        buttonBC = this.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyPlayerBC.IsTouching(buttonBC))
        {
            SceneManager.LoadScene("TimsPrototype", LoadSceneMode.Single);
        }
    }
}
