using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Timothy Harrison on 11/5/22

public class InstantiateMenu : MonoBehaviour
{
    [SerializeField] private PauseMenu pauseMenu;
    private static bool created = false;

    // Start is called before the first frame update
    void Start()
    {
        if (!created)
        {
            Instantiate(pauseMenu);
            created = true;
        }        
    }
}
