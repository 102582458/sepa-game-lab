using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class FireSpread : MonoBehaviour
{
    private float interval = 2;
    private bool[] freeSpaces = { true, true, true, true };
    [SerializeField] private float chance = 1f;
    private int[] fireCheckX = { 1, 0, -1, 0 };
    private int[] fireCheckY = { 0, 1, 0, -1 };

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpreadFire(interval));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator SpreadFire(float interval)
    {
        yield return new WaitForSecondsRealtime(interval);

        //Debug.Log(fireCheckXY.Length);
        for (int i = 0; i < fireCheckX.Length; i++)
        {
            RaycastHit2D fireCheck = Physics2D.Raycast(new Vector2(gameObject.transform.position.x + fireCheckX[i], gameObject.transform.position.y + fireCheckY[i]), Vector2.right, 0);
            if (fireCheck.collider != null)
            {
                Debug.Log(fireCheck.collider.name);
                freeSpaces[i] = false;
            }
            else
            {
                freeSpaces[i] = true;
            }
            //Debug.Log($"freespaces: {freeSpaces[0]}, {freeSpaces[1]}, {freeSpaces[2]}, {freeSpaces[3]}, i: {i}");
        }

        float dieRoll = UnityEngine.Random.Range(0f, 1f);
        //Debug.Log($"Dice Roll: {dieRoll}, Chance: {chance}");
        if (dieRoll < chance)
        {
            List<int> trueIndexes = new List<int>();

            for (int i = 0; i < freeSpaces.Length; i++)
            {
                if (freeSpaces[i])
                {
                    trueIndexes.Add(i);
                }
            }


            //Debug.Log($"false indexes: {falseIndexes[0]}, {falseIndexes[1]}, {falseIndexes[2]}, {falseIndexes[3]}");
            //foreach (int i in falseIndexes)
            //{
            //    Debug.Log($"false index: {i}");
            //}

            System.Random rng = new System.Random();
            int fireSpot = rng.Next(trueIndexes.Count);
            Debug.Log($"firespot: {fireSpot}, trueIndexesCount: {trueIndexes.Count}, freeSpaces: {freeSpaces[0]}, {freeSpaces[1]}, {freeSpaces[2]}, {freeSpaces[3]}");
            //int fireSpot = Random.Range(falseIndexes);
            //Debug.Log($"firespot: {fireSpot}");
            switch (fireSpot)
            {
                case 0:
                    if (freeSpaces[fireSpot])
                    {
                        Instantiate(this, new Vector2(gameObject.transform.position.x + 1, gameObject.transform.position.y), Quaternion.identity);
                        freeSpaces[fireSpot] = false;
                    }
                    break;
                case 1:
                    if (freeSpaces[fireSpot])
                    {
                        Instantiate(this, new Vector2(gameObject.transform.position.x - 1, gameObject.transform.position.y), Quaternion.identity);
                        freeSpaces[fireSpot] = false;
                    }
                    break;
                case 2:
                    if (freeSpaces[fireSpot])
                    {
                        Instantiate(this, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 1), Quaternion.identity);
                        freeSpaces[fireSpot] = false;
                    }
                    break;
                case 3:
                    if (freeSpaces[fireSpot])
                    {
                        Instantiate(this, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 1), Quaternion.identity);
                        freeSpaces[fireSpot] = false;
                    }
                    break;
            }
        }

        StartCoroutine(SpreadFire(interval));
    }
}
