using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<PlayerMovement2D>().comboCounter > 0)
            {
                collision.gameObject.GetComponent<PlayerMovement2D>().comboCounter -= 1;
            }
            collision.transform.position = new Vector2(-8.5f, -1.7f);
        }
    }
}
