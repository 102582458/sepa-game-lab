using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    //Assign the player transform component 
    public Transform player;
    
    // Update is called once per frame
    void LateUpdate()
    {
        //Keeps the camera centered and following the player 
        transform.position = player.transform.position + new Vector3(0, 0, -4);
    }
}
