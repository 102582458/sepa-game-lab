using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    //Initiate variables
    private Rigidbody2D rb;
    [SerializeField] private float enemySpeed = 8f;
    [SerializeField] private float enemyMovementTime = 0.80f;
    [SerializeField] private int enemyDirectionY = 1;

    // Start is called before the first frame update
    private void Awake()
    {
        //Assigns player object components to variables
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(rb.velocity.x, enemySpeed * enemyDirectionY);
        Invoke("ChangeEnemyDirection", enemyMovementTime);
    }

    //Handles all physics updates in the game
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.98f);
    }

    private void ChangeEnemyDirection()
    {
        if (enemyDirectionY == -1)
        {
            enemyDirectionY = 1;
            rb.velocity = new Vector2(rb.velocity.x, enemySpeed * enemyDirectionY);
        } else
        {
            enemyDirectionY = -1;
            rb.velocity = new Vector2(rb.velocity.x, enemySpeed * enemyDirectionY);
        }
        Invoke("ChangeEnemyDirection", enemyMovementTime);
    }
}
