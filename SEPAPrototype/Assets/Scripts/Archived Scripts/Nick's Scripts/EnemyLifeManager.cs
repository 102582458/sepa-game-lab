using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLifeManager : MonoBehaviour
{
    //Assigns variables 
    [SerializeField] private int enemyHealth = 1;
    private int currentEnemyHealth;

    // Start is called before the first frame update
    private void Start()
    {
        //Sets the current health to the maximum health at the start of the game
        currentEnemyHealth = enemyHealth;
    }

    //Function called from PlayerCombat script
    public void TakeDamage(int damage, GameObject obj)
    {
        //Subtracts health from current health based on damage of player
        currentEnemyHealth -= damage;

        //Checks to see if enemy has died and if so, increments the combo counter and triggers the enemyDie function
        if(currentEnemyHealth <= 0)
        {
            obj.GetComponent<PlayerMovement2D>().comboCounter += 1;
            enemyDie();
        }
    }

    //Destroys the enemy from the game
    private void enemyDie()
    {
        Destroy(gameObject);
    }
}
