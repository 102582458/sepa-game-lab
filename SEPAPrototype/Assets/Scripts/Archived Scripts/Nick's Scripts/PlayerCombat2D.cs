using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat2D : MonoBehaviour
{
    //Assigns variables, playerAttackPoint is a child object of player and is used to determine the attack area
    [SerializeField] private Transform playerAttackPoint;
    [SerializeField] private float playerAttackRange = 0.4f;
    [SerializeField] private int playerAttackDamage = 1;
    [SerializeField] private LayerMask enemyLayerMasks;

    // Update is called once per frame
    private void Update()
    {
        //Check wheather or not the attack button, left mouse click, was hit
        if (Input.GetButtonDown("Attack"))
        {
            PlayerAttack();
        }
    }
    
    //Is called everytime the player attacks
    private void PlayerAttack()
    {   
        //Adds all of the enemies within the collision of the circle to an array
        Collider2D[] enemiesInRange = Physics2D.OverlapCircleAll(playerAttackPoint.position, playerAttackRange, enemyLayerMasks);

        //For each enemy within the array, it calls the TakeDamage function on the EnemyLifeManager script 
        foreach(Collider2D enemy in enemiesInRange)
        {
            enemy.GetComponent<EnemyLifeManager>().TakeDamage(playerAttackDamage, this.gameObject);
        }
    }

    //Gives a visual representation of the player hit area when player object is selected, used only for testing
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(playerAttackPoint.position, playerAttackRange);
    }
}
