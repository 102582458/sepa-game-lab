using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGoal : MonoBehaviour
{
    public string loadSceneName = "";

    //Reloads the level when the player reaches the goal
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("NicksPrototype", LoadSceneMode.Single);
        }
    }
}
