using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBounds : MonoBehaviour
{
    //Respawns the player at the starting position
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<PlayerMovement2D>().comboCounter > 0)
            {
                collision.gameObject.GetComponent<PlayerMovement2D>().comboCounter -= 1;
            }
            collision.transform.position = new Vector2(-8.5f, -1.7f);
        }
    }

}
