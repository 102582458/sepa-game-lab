using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2D : MonoBehaviour
{
    //Initiate variables
    private Rigidbody2D rb;
    private CapsuleCollider2D coll;

    //Variables used for player movement
    private float dirAxesX;
    private Vector2 playerVelocity;
    [SerializeField] private float speed = 6f;

    //Variables used for jumping mechanic
    private bool jump;
    [SerializeField] private float jumpHeight = 12f;
    [SerializeField] private LayerMask jumpableTerrain;

    //Variable for the combo counter
    [System.NonSerialized] public int comboCounter = 0;

    //Used to determine the player facing direction
    private bool playerFacingRight = true;

    // Start is called before the first frame update
    private void Awake()
    {
        //Assigns player object components to variables
        rb = GetComponent<Rigidbody2D>();
        coll = GetComponent<CapsuleCollider2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        //Determines input direction of player and velocity 
        dirAxesX = Input.GetAxisRaw("Horizontal");
        playerVelocity = new Vector2(dirAxesX * speed, rb.velocity.y);

        //Determines if the input for jump is triggered and does a check to see if the player is on jumpable terrain
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            jump = true;
        } else if (Input.GetButtonDown("Jump") && comboCounter > 0) //Determines if the player is using a combo point for an extra jump
        {
            jump = true;
            comboCounter -= 1;
        }
    }
    
    //Handles all physics updates in the game
    private void FixedUpdate()
    {
        //Sets the player velocity
        rb.velocity = playerVelocity;

        //Determines what direction the player is and should be facing
        if (dirAxesX < 0 && playerFacingRight)
        {
            FlipPlayer();
        } else if (dirAxesX > 0 && !playerFacingRight)
        {
            FlipPlayer();
        }

        //Alters the velocity of the player to allow the player to jump
        if (jump)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
            //rb.velocity = new Vector2(rb.velocity.x, jumpHeight); - Uses velocity instead of force
            jump = false;
        }
    }

    //Changes the player rotation depending on which direction the player is running
    private void FlipPlayer()
    {
        playerFacingRight = !playerFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }

    //Uses a boxcast below the players collision box to check if the player is on jumpable terrain
    private bool IsGrounded()
    {
        //return Physics2D.BoxCast(coll.bounds.center, coll.bounds.size - new Vector3(0.1f, 0f, 0f), 0f, Vector2.down, .1f, jumpableTerrain); - Uses boxcast instead
        return Physics2D.CapsuleCast(coll.bounds.center, coll.bounds.size, CapsuleDirection2D.Vertical, 0f, Vector2.down, .1f, jumpableTerrain);
    }
}
