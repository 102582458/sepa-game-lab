using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    //Written by Nicholas Mueller on 26/10/22

    [SerializeField] private Rigidbody2D rb;
    private Vector3 initialPos;
    private float asteroidTimer = 0f;
    [SerializeField] private float minXVelocity = -5f, maxXVelocity = -10f, minYVelocity = -5f, maxYVelocity = 5f;
    [SerializeField] private float resetTimer = 10f;

    //Stores the inital position of the asteroid for reference in the update function when resetting it
    private void Awake()
    {
        initialPos = transform.position;
    }

    private void Update()
    {
        if (asteroidTimer > resetTimer) //Resets the asteroids velocity and position when it goes beyond the resetTimer
        {
            rb.velocity = Vector3.zero;
            transform.position = initialPos;
            asteroidTimer = 0f;
        } else if (asteroidTimer == 0f) //Sets a new random velocity 
        {
            rb.velocity = new Vector2(Random.Range(minXVelocity, maxXVelocity), Random.Range(minYVelocity, maxYVelocity));
            asteroidTimer += Time.deltaTime;
        } else //Increments timer
        {
            asteroidTimer += Time.deltaTime;
        }
    }
}
