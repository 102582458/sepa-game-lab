using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Written by Timothy Harrison on 11/5/22
public class VolumeSlider : MonoBehaviour
{
    //Array of UI sliders and a reference to the sound manager
    private Slider[] sliders;
    private SoundManager soundManager;

    // Start is called before the first frame update - gets sliders and soundmanager scipt, sets starting slider values determined by soundmanager
    void Start()
    {
        sliders = gameObject.GetComponentsInChildren<Slider>();
        
        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        sliders[0].value = soundManager.GetMasterVolume();
        sliders[1].value = soundManager.GetMusicVolume();
        sliders[2].value = soundManager.GetSfxVolume();
    }

    // Update is called once per frame - changes the volume variables in sound manager when the slider changes
    void Update()
    {
        soundManager.SetMaserVolume(sliders[0].value);
        soundManager.SetMusicVolume(sliders[1].value);
        soundManager.SetSfxVolume(sliders[2].value);
    }
}
