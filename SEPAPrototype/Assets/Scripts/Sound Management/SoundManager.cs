using System.Collections;
using System.Collections.Generic;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.SceneManagement;

//Written by Tim Harrison on 7/5/22
//Edited by Tim Horrison on 11/5/22
//Edited by Tim Harrison on 13/8/22 - Updated to use 'playoneshot' for sfx so multiple sfx can play at the same time
//Edited by Tim Harrison on 26/8/22 - updated to only play one ice sfx at a time
//Edited by Tim Harrison on 18/10/22 - added FE sfx functionality and ability to stop playing from an audio source
//Edited by Tim Harrison on 18/10/22 - add functionality for multiple BGM for different levels
//Edited by Tim Harrison on 30/10/22 - brought music volume back up to 0.5 and added a stopallcoroutines call to PlayMusic()
//Edited by Tim Harrison on 30/10/22 - boss sfx implementation (scream and explosion)
//Edited by Tim Harrison on 8/11/22 - updated bgm to loop without stopping

public class SoundManager : MonoBehaviour
{
    
    [SerializeField] private List<string> audioName;
    [SerializeField] private List<AudioClip> audioClips;
    [SerializeField] private List<string> bgmName;
    [SerializeField] private List<AudioClip> bgmClips;

    private Dictionary<string, AudioClip> audioDict = new();
    private Dictionary<string, AudioClip> bgmDict = new();
    private float sfxVolume = 0.5f;
    private float musicVolume = 0.5f;
    private float masterVolume = 0.5f;
    private int trackNumber = 0;

    private bool canPlayIceSound = true;
    private bool canPlayFESound = true;
    private bool canPlayBossScream = true;
    private bool loopLevelBGM = true;

    private string bossLevel = "Level12";

    private AudioSource[] audioSources;
    private static SoundManager soundManagerInstance;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if (soundManagerInstance == null)
        {
            soundManagerInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        audioSources = GetComponents<AudioSource>();
        //Set up dictionaries of audio clips
        audioDict = CreateAudioDict(audioName, audioClips, audioDict, "SFX");
        bgmDict = CreateAudioDict(bgmName, bgmClips, bgmDict, "BGM");

        //add delegate to get notification when scene is loaded
        SceneManager.sceneLoaded += OnSceneLoad;
    }

    //changes voluem of audio source based on the volume variables
    private void Update()
    {
        audioSources[0].volume = sfxVolume * masterVolume;
        audioSources[1].volume = musicVolume * masterVolume;
    }

    private void OnSceneLoad(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        PlayMusic();
    }

    //Sets up dictionaires of audio clips
    private Dictionary<string, AudioClip> CreateAudioDict(List<string> names, List<AudioClip> clips, Dictionary<string, AudioClip> dict, string audioType)
    {
        if (clips.Count == names.Count)
        {
            for (int i = 0; i < clips.Count; i++)
            {
                dict.Add(names[i], clips[i]);
            }
        }
        else
        {
            Debug.Log($"ERROR: NOT ALL AUDIO FILES HAVE REFERENCE STRING - {audioType}");
        }

        return dict;
    }

    public void PlaySound(string sound)
    {
        if (sound == "iceSlip1" || sound == "iceSlip2" || sound == "dialogue")
        {
            //checks if ice sfx is playing and doesnt play if it is
            if (canPlayIceSound)
            {
                audioSources[0].clip = audioDict[sound];
                audioSources[0].PlayOneShot(audioDict[sound]);                
                StartCoroutine(waitForIceSfx(audioDict[sound]));
            }
        }
        else if (sound == "fireExtinguisher")
        {
            if (canPlayFESound)
            {
                audioSources[0].clip = audioDict[sound];
                audioSources[0].Play();
                StartCoroutine(waitForFESfx(audioDict[sound]));
            }            
        }
        else if (sound == "bossScream")
        {
            if (canPlayBossScream)
            {
                audioSources[0].clip = audioDict[sound];
                audioSources[0].PlayOneShot(audioDict[sound]);
                StartCoroutine(waitForBossScreamSFX(audioDict[sound]));
            }
        }
        else
        {
            audioSources[0].clip = audioDict[sound];
            audioSources[0].PlayOneShot(audioDict[sound]);
        }        
    }

    //Stops the audiosource in the list using index i
    public void StopSound(int i)
    {
        audioSources[i].Stop();
    }

    private IEnumerator waitForIceSfx(AudioClip sound)
    {
        canPlayIceSound = false;

        yield return new WaitForSeconds(sound.length);

        canPlayIceSound = true;
    }

    //code to loop FE sfx
    private IEnumerator waitForFESfx(AudioClip sound)
    {
        canPlayFESound = false;

        yield return new WaitForSeconds(sound.length);

        canPlayFESound = true;
    }

    //Plays only one boss scream at a time
    private IEnumerator waitForBossScreamSFX(AudioClip sound)
    {
        canPlayBossScream = false;

        yield return new WaitForSeconds(sound.length);

        canPlayBossScream = true;
    }

    //Plays BGM depending on the current level
    private void PlayMusic()
    {
        AudioSource[] audioSources = soundManagerInstance.GetComponents<AudioSource>();
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "MainMenu")
        {
            PlayMusicClip("MainMenuBGM", audioSources[1]);          
        }
        else if (sceneName == bossLevel)
        {
            PlayMusicClip("BossBGM", audioSources[1]);
        }
        else
        {
            PlayMusicClip("LevelBGM", audioSources[1]);
        }
    }

    //Assigns new clip to audio source and plays it
    private void PlayMusicClip(string bgmName, AudioSource audioSource)
    {
        if (audioSource.clip != bgmDict[bgmName])
        {
            audioSource.clip = bgmDict[bgmName];
            audioSource.Play();
            trackNumber++;
            StartCoroutine(WaitForBGMLoop(trackNumber));
        }
    }

    //Plays music again if looping
    private void PlayMusicLooped(int trackNumber)
    {
        AudioSource[] audioSources = soundManagerInstance.GetComponents<AudioSource>();
        audioSources[1].Play();
        StartCoroutine(WaitForBGMLoop(trackNumber));
    }

    //Waits for clip length and replays it if same track just ended
    private IEnumerator WaitForBGMLoop(int trackNumber)
    {
        yield return new WaitForSeconds(audioSources[1].clip.length);
        if (trackNumber == this.trackNumber)
        {
            PlayMusicLooped(trackNumber);
        }
    }


    //Getters and setters for volume variables
    public float GetSfxVolume()
    {
        return sfxVolume;
    }

    public float GetMusicVolume()
    {
        return musicVolume;
    }

    public float GetMasterVolume()
    {
        return masterVolume;
    }

    public void SetSfxVolume(float f)
    {
        sfxVolume = f;
    }

    public void SetMusicVolume(float f)
    {
        musicVolume = f;
    }

    public void SetMaserVolume(float f)
    {
        masterVolume = f;
    }
}
