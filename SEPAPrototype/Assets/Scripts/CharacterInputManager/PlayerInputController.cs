using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

//Originally written by Nicholas Mueller - 22/5/22 - Handles the player input controls for the player characters to be referenced by other scripts 
//Edited by Nicholas Mueller - 23/08/2022 - Updated some logical statements to address bugs  

public class PlayerInputController : MonoBehaviour
{
    //Declare variables
    private PlayerControls playerBlueControls, playerRedControls;
    private int playerBlueHash, playerRedHash;
    private int currentGamepadCount;
    private static PlayerInputController PlayerInputControllerInstance;

    private string player1Input, player2Input = "Keyboard";

    private void Awake()
    {
        //Sets values for variables
        playerBlueControls = new PlayerControls();
        playerRedControls = new PlayerControls();
        playerBlueHash = 0;
        playerRedHash = 0;
        currentGamepadCount = -1;

        DontDestroyOnLoad(gameObject);

        if (PlayerInputControllerInstance == null)
        {
            PlayerInputControllerInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //Enables the player controls
    private void OnEnable()
    {
        playerBlueControls.Enable();
        playerRedControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        playerBlueControls.Disable();
        playerRedControls.Disable();
    }

    private void Update()
    {   
        //Disables player controls when the game is paused
        if (PauseMenu.gameIsPaused)
        {
            OnDisable();
        }
        else
        {
            OnEnable();
        }

        //Calls the SetCharacterInput function to detect updates for the player controls
        if (currentGamepadCount != Gamepad.all.Count)
        {
            currentGamepadCount = Gamepad.all.Count;
            if (Gamepad.all.Count == 0)
            {
                SetCharacterInput(0);
            }
            else if (Gamepad.all.Count == 1)
            {
                SetCharacterInput(1);
            }
            else if (Gamepad.all.Count >= 2)
            {
                SetCharacterInput(2);
            }
        }
    }

    public void SetCharacterInput(int gamepadCount)
    {        
        //Sets the hash values of the player gamepads to reference them on disconnects and reconnects
        if (gamepadCount != 0)
        {
            if (playerBlueHash == 0)
            {
                playerBlueControls.devices = new[] { Gamepad.all[0] };
                playerBlueHash = Gamepad.all[0].description.GetHashCode();
            }
            if (playerRedHash == 0)
            {
                if (gamepadCount == 1 && playerBlueHash != Gamepad.all[0].description.GetHashCode())
                {
                    playerRedControls.devices = new[] { Gamepad.all[0] };
                    playerRedHash = Gamepad.all[0].description.GetHashCode();
                }
                else if (gamepadCount == 2)
                {
                    playerRedControls.devices = new[] { Gamepad.all[1] };
                    playerRedHash = Gamepad.all[1].description.GetHashCode();
                }
            }
        }

        //Sets the player controls 
        switch (gamepadCount)
        {
            case 1:
                if (playerBlueHash == Gamepad.all[0].description.GetHashCode() && player1Input == "Gamepad")
                {
                    playerBlueControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerBlueControls.devices = new[] { Gamepad.all[0] };
                }
                else
                {
                    playerBlueControls.devices = new[] { Keyboard.current };
                    playerBlueControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                }

                if (playerRedHash == Gamepad.all[0].description.GetHashCode() && player2Input == "Gamepad")
                {
                    playerRedControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerRedControls.devices = new[] { Gamepad.all[0] };
                }
                else
                {
                    playerRedControls.devices = new[] { Keyboard.current };
                    playerRedControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                }
                break;
            case 2:
                if (playerBlueHash == Gamepad.all[0].description.GetHashCode() && player1Input == "Gamepad")
                {
                    playerBlueControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerBlueControls.devices = new[] { Gamepad.all[0] };
                }
                else if (playerBlueHash == Gamepad.all[1].description.GetHashCode() && player1Input == "Gamepad")
                {
                    playerBlueControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerBlueControls.devices = new[] { Gamepad.all[1] };
                }
                else
                {
                    playerBlueControls.devices = new[] { Keyboard.current };
                    playerBlueControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                }

                if (playerRedHash == Gamepad.all[0].description.GetHashCode() && player2Input == "Gamepad")
                {
                    playerRedControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerRedControls.devices = new[] { Gamepad.all[0] };
                }
                else if (playerRedHash == Gamepad.all[1].description.GetHashCode() && player2Input == "Gamepad")
                {
                    playerRedControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
                    playerRedControls.devices = new[] { Gamepad.all[1] };
                }
                else
                {
                    playerRedControls.devices = new[] { Keyboard.current };
                    playerRedControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                }
                break;
            default:
                playerBlueControls.devices = new[] { Keyboard.current };
                playerRedControls.devices = new[] { Keyboard.current };
                playerBlueControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                playerRedControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
                break;
        }
        /*
        if (player1Input == "Keyboard" || player1Input == null)
        {
            playerBlueControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
        }
        else if (player1Input == "Gamepad")
        {
            playerBlueControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
        }

        if (player2Input == "Keyboard" || player2Input == null)
        {
            playerRedControls.bindingMask = InputBinding.MaskByGroup("Keyboard");
        }
        else if (player2Input == "Gamepad")
        {
            playerRedControls.bindingMask = InputBinding.MaskByGroup("Gamepad");
        }
        */
    }

    //Getter's to obtain the player controls in other scripts
    public PlayerControls GetBlueCharacterControls()
    {
        return playerBlueControls;
    }

    public PlayerControls GetRedCharacterControls()
    {
        return playerRedControls;
    }

    public int GetPlayer1Input()
    {
        if (player1Input == "Keyboard")
        {
            return 0;
        }
        else if (player1Input == "Gamepad")
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int GetPlayer2Input()
    {
        if (player2Input == "Keyboard")
        {
            return 0;
        }
        else if (player2Input == "Gamepad")
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public void SetPlayer1Input(string i)
    {
        player1Input = i;
        SetCharacterInput(currentGamepadCount);
    }

    public void SetPlayer2Input(string i)
    {
        player2Input = i;
        SetCharacterInput(currentGamepadCount);
    }
}
