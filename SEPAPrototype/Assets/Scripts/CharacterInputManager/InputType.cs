using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputType : MonoBehaviour
{
    private TMPro.TMP_Dropdown[] inputTypeValue;
    private PlayerInputController playerInputController;
    [SerializeField] private int player;

    // Start is called before the first frame update
    void Start()
    {
        playerInputController = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>();

        inputTypeValue = GetComponentsInChildren<TMPro.TMP_Dropdown>();

        if (player == 1)
        {
            inputTypeValue[0].value = playerInputController.GetPlayer1Input();
        }
        else if (player == 2)
        {
            inputTypeValue[0].value = playerInputController.GetPlayer2Input();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player == 1)
        {
            playerInputController.SetPlayer1Input(inputTypeValue[0].captionText.text);
        }
        else if (player == 2)
        {
            playerInputController.SetPlayer2Input(inputTypeValue[0].captionText.text);
        }
    }
}
