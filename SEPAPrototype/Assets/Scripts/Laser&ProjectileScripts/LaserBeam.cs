using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - 29/4/22
//Edited by Tim Harrison on 9/9/22
//Edited by Tim Harrison on 23/9/22 - created layermask with fires and red and blue zones that is passed into the raycast so it ignores them
//Edited by Tim Harrison on 23/9/22 - gave line renderer sorting order to have it show over other objects in the scene
//Edited by Tim Harrison on 25/9/22 - kills player if touches them
//Edited by Tim Harrison on 26/9/22 - Deals damage to boss

public class LaserBeam
{
    private Vector2 pos, dir;

    private GameObject laserObj;
    private LineRenderer laser;
    private List<Vector2> laserIndices = new List<Vector2>();
    private GameObject lastTouchedMirror;
    private LayerMask layerMask;
    private LayerMask layerMask2;
    private LayerMask layerMask3;
    private LayerMask layerMask4;
    private LayerMask finalMask;

    private BossBehaviour bossBehaviour;
    private ShootLaser laserPointer;
    private bool hitBoss;

    //Constructor for laser beam
    public LaserBeam(Vector2 pos, Vector2 dir, Material material, ShootLaser laserPointer)
    {
        this.laser = new LineRenderer();
        this.laserObj = new GameObject();
        this.laserObj.name = "laser beam";
        this.laserObj.tag = "Laser";
        this.pos = pos;
        this.dir = dir;

        this.laser = laserObj.AddComponent(typeof(LineRenderer)) as LineRenderer;
        this.laser.startWidth = 0.1f;
        this.laser.endWidth = 0.1f;
        this.laser.material = material;
        this.laser.startColor = Color.green;
        this.laser.endColor = Color.green;
        this.laser.sortingOrder = 10;

        this.bossBehaviour = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossBehaviour>();
        this.laserPointer = laserPointer;
        hitBoss = laserPointer.GetHitBoss();


        layerMask = 1 << 12;
        layerMask2 = 1 << 13;
        layerMask3 = 1 << 18;
        layerMask4 = 1 << 19;

        finalMask = layerMask | layerMask2 | layerMask3 | layerMask4;

        CastRay(pos, dir);
    }

    //Casts a ray out up to 30 units, calls different methods if it hits object or goes full 30 units
    private void CastRay(Vector2 pos, Vector2 dir)
    {
        laserIndices.Add(pos);

        Ray2D ray = new (pos, dir);
        RaycastHit2D hit = Physics2D.Raycast(pos, dir, 30, ~finalMask);

        if (hit)
        {
            //Checks if touching player, and tells them to die if so
            if (hit.transform.tag == "Player")
            {
                PlayerDeath playerDeath = hit.transform.GetComponent<PlayerDeath>();
                playerDeath.KillPlayer();
            }

            //Checks if raycast hits the boss and increments if it didnt hit with previous laser
            if (hit.transform.tag == "Boss" && !hitBoss)
            {                
                bossBehaviour.incrementDamage();
                laserPointer.SetHitBoss(true);
            }
            //Decrements boss damage if not hitting and previous laser did hit
            else if (hit.transform.tag != "Boss" && hitBoss)
            {
                bossBehaviour.decrementDamage();
                laserPointer.SetHitBoss(false);
            }

            CheckHit(hit, dir);
        }
        else
        {
            laserIndices.Add(ray.GetPoint(30));
            UpdateLaser();
        }
    }

    //Final method called a creates laser path based on the vertexs created from hitting objects
    private void UpdateLaser()
    {
        int count = 0;

        laser.positionCount = laserIndices.Count;

        foreach (Vector2 idx in laserIndices)
        {
            laser.SetPosition(count, idx);
            count++;
        }
    }

    //Checks if hits a mirror and recalls 'castray' with new direction or calls updatelaser to finish the laser
    private void CheckHit(RaycastHit2D hitInfo, Vector2 direction)
    {
        if (hitInfo.collider.gameObject.tag == "Mirror" && hitInfo.collider.gameObject != lastTouchedMirror)
        {
            lastTouchedMirror = hitInfo.collider.gameObject;

            Vector2 pos = hitInfo.point + hitInfo.normal/1000;
            Vector2 dir = Vector2.Reflect(direction, hitInfo.normal);

            CastRay(pos, dir);
        }
        else
        {
            laserIndices.Add(hitInfo.point);
            UpdateLaser();
        }
    }

    public GameObject GetLaserObj()
    {
        return this.laserObj;
    }
}
