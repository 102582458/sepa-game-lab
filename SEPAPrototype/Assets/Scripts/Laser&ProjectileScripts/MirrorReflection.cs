using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on /date unknown/
//Edited by Tim Harrison on 9/5/22 - Added RotateMirror() method to implement player input driven rotation
//Edited by Tim Harrison on 18/10/22 - Added rotation sfx

public class MirrorReflection : MonoBehaviour, iInteract
{
    [SerializeField] private bool orientationIsLeft;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    private void Start()
    {
        //sound manager setup
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }

        if (orientationIsLeft)
        {
            gameObject.transform.rotation = Quaternion.identity;
        }
        else
        {
            gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }
    }

    public void Interact(Dictionary<string, object> interactParameters)
    {
        PlaySound("mirrorTurn");

        if (orientationIsLeft)
        {
            gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            orientationIsLeft = false;
        }
        else
        {
            gameObject.transform.rotation = Quaternion.identity;
            orientationIsLeft = true;
        }
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    public bool GetOrientationIsLeft()
    {
        return orientationIsLeft;
    }
}
