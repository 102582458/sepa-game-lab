using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Reworked by Tim Harrison on 11/5/22
//Edited by Tim Harrison on 12/5/22 - has sprite variables for red and blue versions, faction variable added with setter, added "setSprite" function to determine active sprite based on faction
//Edited by Tim Harrison on 20/8/22 - added call to sound manager for projectile receive sound, updated laser door sound call to use tag instead of name
//Edite on 22/08/22 by Tim Harrison - Updated to use new parent script

public class ProjectileReceiver : ButtonBase
{
    [SerializeField] private Sprite activeSpriteRed;
    [SerializeField] private Sprite activeSpriteBlue;

    private ProjectileReceiver otherReceiverS;

    private string faction;
    private string projectileTag = "Projectile";


    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        SetUpSound();
        SetUpTileManager();
        //this method returns an object so you need to use an explicit cast to convert it to the object you want
        otherReceiverS = (ProjectileReceiver)CheckPairedObject(otherReceiverS);
        
    }
    

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == projectileTag)
        {
            PlaySound("projectileReceive");

            faction = collision.gameObject.GetComponent<ProjectileBehaviour>().GetFaction();
            if (!isDependant)
            {
                SetSprite();
                SetState();
            }
            else
            {
                colliding = true;
                SetSprite();
                //timer = activeTime;
                if (otherReceiverS.colliding)
                {
                    SetState();
                }
            }            
        }
    }

    //checks if there is a paired receiver and gets the script from it
    protected override object CheckPairedObject(Object otherObjectS)
    {
        if (pairedObject.Length == 1)
        {
            otherObjectS = pairedObject[0].GetComponent<ProjectileReceiver>();
            return otherObjectS;
        }
        else
        {
            return null;
        }
    }

    //Sets the sprite of the receiver based on the color of the projectile
    protected void SetSprite()
    {
        if (faction == "red")
        {
            spriteRenderer.sprite = activeSpriteRed;
        }
        else
        {
            spriteRenderer.sprite = activeSpriteBlue;
        }
    }    

    //resets receiver values to deactive state
    public void Reset()
    {
        if (!completed)
        {
            colliding = false;
            spriteRenderer.sprite = deactiveSprite;
        }        
    }
}
