using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - 30/4/22
//Edited by Tim Harrison on 12/5/22 - Added sprites for red and blue active states and projectiles, added if statement to set active sprite based on faction for shooter and projectile
//Edited by Tim Harrison on 17/5/22 - is now uses iInteract interface and the associated interact function instead of previous UseProjectileShooter function
//Edited by Tim Harrison on 8/3/22 - Added projectileSpeed variable, passes this variable through as projectile parameter
//Edited by Tim Harrison on 20/8/22 - Added reference to sound manager and calls it to play shoot projectile sound
//Edited by Tim Harrison on 9/9/22 - Added enum and serialized field for setting initial direction. Now pass through left or right direction ((-)transform.right) in dir field when creating projectile and pass through initial direction as string

public class ShootProjectile : MonoBehaviour, iInteract
{
    private enum InitialDirection
    {
        right,
        left,
    }
    [SerializeField] private InitialDirection initialDirection;

    //The sprite we wish to attach to the projectile
    [SerializeField] private Sprite projectileSpriteRed;
    [SerializeField] private Sprite projectileSpriteBlue;
    [SerializeField] private Sprite deactiveSprite;
    [SerializeField] private Sprite activeSpriteRed;
    [SerializeField] private Sprite activeSpriteBlue;

    [SerializeField] private int numberOfShots;
    [SerializeField] private float waitTime;
    [SerializeField] private float projectileSpeed;

    [SerializeField] private Transform projectileSpawn;
    [SerializeField] private List<GameObject> projectileReceivers;

    private List<Projectile> currentProjectiles = new List<Projectile>();
    private bool firing = false;
    private SpriteRenderer spriteRenderer;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = deactiveSprite;
    }

    public void Interact(Dictionary<string, object> interactParameters)
    {
        if (!firing)
        {
            foreach (GameObject g in projectileReceivers)
            {
                g.GetComponent<ProjectileReceiver>().Reset();
            }

            firing = true;

            if (currentProjectiles.Count != 0)
            {
                for (int i = 0; i < currentProjectiles.Count; i++)
                {
                    Destroy(currentProjectiles[i].GetProjectileObj());
                }

                currentProjectiles.Clear();
            }

            //sets active sprites depending on faction
            if ((string)interactParameters["faction"] == "red")
            {
                spriteRenderer.sprite = activeSpriteRed;
                StartCoroutine(Shoot(projectileSpriteRed, (string)interactParameters["faction"]));
            }
            else if ((string)interactParameters["faction"] == "blue")
            {
                spriteRenderer.sprite = activeSpriteBlue;
                StartCoroutine(Shoot(projectileSpriteBlue, (string)interactParameters["faction"]));
            }
        }
    }

    //is a coroutine that runs on a different thread so the code can wait without stopping the rest of the code
    private IEnumerator Shoot(Sprite projectileSprite, string faction)
    {
        for (int i = 0; i < numberOfShots; i++)
        {
            PlaySound("projectileShoot");

            Vector3 direction = initialDirection == InitialDirection.right ? gameObject.transform.right : -gameObject.transform.right;

            Projectile projectile = new(projectileSpawn.position, direction, projectileSprite, faction, projectileSpeed, initialDirection.ToString());
            currentProjectiles.Add(projectile);

            yield return new WaitForSeconds(waitTime);
        }

        firing = false;
        spriteRenderer.sprite = deactiveSprite;
    }

    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }
}
