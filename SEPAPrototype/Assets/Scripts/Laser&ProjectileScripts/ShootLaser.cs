using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - 29/4/22
//Edited by Tim Harrison on 26/9/22 - stores variable for if the laser is hitting the boss or not

public class ShootLaser : MonoBehaviour
{
    public Material material;
    private LaserBeam beam;
    private bool hitBoss;

    private void Start()
    {
        hitBoss = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (beam != null)
        {
            Destroy(beam.GetLaserObj());
        }
        beam = new LaserBeam(gameObject.transform.position, gameObject.transform.right, material, this);
    }

    //Getter for hitBoss tag
    public bool GetHitBoss()
    {
        return hitBoss;
    }

    //Setter for hitBoss tag
    public void SetHitBoss(bool value)
    {
        hitBoss = value;
    }
}
