using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on /date unknown/
//Edited by Tim Harrison on 11/5/22 - reworked code to use list of directions instead of dictionary and rotate object instead of changing direction
//Edited by Tim Harrison on 12/5/22 - add faction variable with getter and setter
//Edited by Tim Harrison on 3/8/22 - removed assigning value to moveSpeed var and added setter for it instead
//Edited by Tim Harrison on 20/8/22 - added reference to sound manager and call to play sound when hit mirror
//Edited by Tim Harrison on 9/9/22 - current direction is now set with setter and is updated to face left in start() if its facing that direction (default is right)

public class ProjectileBehaviour : MonoBehaviour
{
    private Vector2 pos, dir;
    private string currentDirection;

    private GameObject lastTouchedMirror;
    private MirrorReflection mirrorS;
    private readonly string[] direction = { "right", "up", "left", "down" };

    private float moveSpeed;
    private bool orientation;
    private bool hittingMirror;
    private string faction;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    // Start is called before the first frame update
    void Start()
    {
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }

        Physics2D.IgnoreLayerCollision(17, 17);
        Physics2D.IgnoreLayerCollision(17, 18);

        transform.position = pos;

        if (currentDirection == direction[2])
        {
            gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!hittingMirror)
        {
            transform.Translate(Time.deltaTime * moveSpeed * dir);
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, lastTouchedMirror.transform.position, moveSpeed * Time.deltaTime);

            if (transform.position == lastTouchedMirror.transform.position)
            {
                hittingMirror = false;

                ChangeDirection();
            }
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Mirror")
        {
            PlaySound("mirrorHit");

            hittingMirror = true;
            lastTouchedMirror = collision.gameObject;
            mirrorS = collision.gameObject.GetComponent<MirrorReflection>();
            orientation = mirrorS.GetOrientationIsLeft();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {       
        Destroy(gameObject);
    }

    //checks current direction and rotates object to new direction
    private void ChangeDirection()
    {
        if (orientation)
        {
            if (currentDirection == direction[0])
            {
                currentDirection = direction[3];
                gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
            }
            else if (currentDirection == direction[3])
            {
                currentDirection = direction[0];
                gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (currentDirection == direction[2])
            {
                currentDirection = direction[1];
                gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else
            {
                currentDirection = direction[2];
                gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }
        else
        {
            if (currentDirection == direction[0])
            {
                currentDirection = direction[1];
                gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else if (currentDirection == direction[3])
            {
                currentDirection = direction[2];
                gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }
            else if (currentDirection == direction[2])
            {
                currentDirection = direction[3];
                gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
            }
            else
            {
                currentDirection = direction[0];
                gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }
    }

    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    public void SetPos(Vector2 position)
    {
        pos = position;
    }

    public void SetDir(Vector2 direction)
    {
        dir = direction;
    }

    public void SetFaction(string s)
    {
        faction = s;
    }

    public string GetFaction()
    {
        return faction;
    }

    public void SetMoveSpeed(float f)
    {
        moveSpeed = f;
    }

    public void SetInitialDirection(string direction)
    {
        currentDirection = direction;
    }
}
