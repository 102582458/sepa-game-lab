using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison on 8/9/22
//Edited by Tim Harrison on 23/9/22 - Added functionality to pause laser movement
//Edited by Tim Harrison on 25/9/22 - Added functionality to pause movement on start
//Edited by Tim Harrison on 25/10/22 - Added public functions for buttons to call and pause lasers


public class LaserMovement : MonoBehaviour
{
    public enum TypeOfMovement
    {
        Sweeping,
        Rotating,
        SweepingRotation,
        Stationary,
    }

    private enum InitialDirection
    {
        DecreasingCoordinate,
        IncreasingCoordinate,
    }

    private enum Orientation
    {
        Vertical,
        Horizontal,
    }

    [SerializeField] private TypeOfMovement typeOfMovement;    
    [SerializeField] private InitialDirection initialDirection;

    [Header("Sweeping attributes")]
    [SerializeField] private Orientation orientation;
    [SerializeField] private int minDistance;
    [SerializeField] private int maxDistance;    
    [SerializeField] private float tilesPerSecond;
    [SerializeField] private bool pauseOnStart;
    [SerializeField] private float pauseOnStartDuration;
    [SerializeField] private bool pauseLaserMovement;
    [SerializeField] private List<int> pauseLocations;
    [SerializeField] private List<float> pauseDurations;    

    [Header("Rotating Attributes")]
    [SerializeField] private float rotationAngle;
    [SerializeField] private float rotationalFrequency;

    private bool decreasingPosition;
    private Rigidbody2D rb;
    private float previousAngle;
    private Vector2 previousPosition;


    // Start is called before the first frame update
    void Start()
    {
        //assigns bool based on enum selected in inspector
        decreasingPosition = initialDirection == InitialDirection.DecreasingCoordinate ? true : false;
        rb = GetComponent<Rigidbody2D>();
        previousAngle = 0;
        previousPosition = transform.position;

        if (pauseOnStart)
        {
            StartCoroutine(WaitForPause(pauseOnStartDuration, CheckMovementForPause()));
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (typeOfMovement)
        {
            case TypeOfMovement.Sweeping:
                UpdateLaserPosition();
                break;
            case TypeOfMovement.Rotating:
                UpdateLaserRotation();
                break;
            case TypeOfMovement.SweepingRotation:
                UpdateLaserPosition();
                UpdateLaserRotation();
                break;
            case TypeOfMovement.Stationary:
                UpdateLaserStationary();
                break;
        }        
    }

    private void UpdateLaserStationary()
    {
        rb.velocity = new Vector2(0, 0);
    }

    //Uses a sin wave to increment the angle of the laser
    private void UpdateLaserRotation()
    {
        float angle = Mathf.Sin(Time.time*rotationalFrequency) * rotationAngle;
        float increment = angle - previousAngle;
        previousAngle = angle;

        transform.Rotate(0, 0, increment);
    }

    //Calls the movement method with parameters dependant on whether the laser is moving horizontally or vertically
    private void UpdateLaserPosition()
    {
        switch (orientation)
        {
            case Orientation.Horizontal:
                VectorMovement(transform.position.x, tilesPerSecond, 0);
                break;

            case Orientation.Vertical:
                VectorMovement(transform.position.y, 0, tilesPerSecond);
                break;
        }
    }

    //Moves laser between min and max coordinates
    private void VectorMovement(float currentPosition, float xVector, float yVector)
    {
        if (pauseLaserMovement)
        {
            PauseMovement(currentPosition);
        }

        if (currentPosition > minDistance && decreasingPosition)
        {
            rb.velocity = new Vector2(-xVector, -yVector);
        }
        else if (currentPosition < maxDistance && !decreasingPosition)
        {
            rb.velocity = new Vector2(xVector, yVector);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }        

        SetDirection(currentPosition);
    }

    //Sets the direction of the laser towards min or max coordinate
    private void SetDirection(float currentPosition)
    {
        if (currentPosition <= minDistance)
        {
            decreasingPosition = false;
        }
        else if (currentPosition >= maxDistance)
        {
            decreasingPosition = true;
        }
    }

    //Checks the orientation of laser and then calls method to check if its in the pause position
    private void PauseMovement(float currentPosition)
    {
        int j = 0;
        switch (orientation)
        {
            case Orientation.Vertical:
                foreach (int i in pauseLocations)
                {
                    CheckPosition(currentPosition, previousPosition.y, i, j);
                    j++;
                }
                previousPosition = new Vector2(transform.position.x, currentPosition);
                break;

            case Orientation.Horizontal:
                foreach (int i in pauseLocations)
                {
                    CheckPosition(currentPosition, previousPosition.x, i, j);
                    j++;
                }
                previousPosition = new Vector2(currentPosition, transform.position.y);
                break;
        }        
    }

    //Checks if laser is in pause position, and changes its movement if so and call IEnumorator to wait for pause duration
    private void CheckPosition(float currentPosition, float previousPosition, int pauseLocation, int index)
    {
        if ((previousPosition < pauseLocation && currentPosition >= pauseLocation) || (previousPosition > pauseLocation && currentPosition <= pauseLocation))
        {
            StartCoroutine(WaitForPause(pauseDurations[index], CheckMovementForPause()));
        }
    }

    //Sets current movement and returns previous movement
    private TypeOfMovement CheckMovementForPause()
    {
        TypeOfMovement previousMovement = typeOfMovement;
        
        switch (typeOfMovement)
        {
            case TypeOfMovement.Sweeping:
                typeOfMovement = TypeOfMovement.Stationary;
                break;
            case TypeOfMovement.SweepingRotation:
                UpdateLaserStationary();
                typeOfMovement = TypeOfMovement.Rotating;
                break;
            default:
                typeOfMovement = TypeOfMovement.Stationary;
                break;
        }

        return previousMovement;
    }

    //Wait for pause duration and then reset movement type back to its original state (if time is 0 or less, laser will pause forever)
    private IEnumerator WaitForPause(float duration, TypeOfMovement previousMovementType)
    {
        if (duration > 0)
        {
            yield return new WaitForSeconds(duration);

            typeOfMovement = previousMovementType;
        }        
    }

    //Pauses laser when called
    public TypeOfMovement PauseLaser()
    {
        TypeOfMovement previousMovement = typeOfMovement;
        typeOfMovement = TypeOfMovement.Stationary;

        return previousMovement;
    }

    //Unpauses laser when called
    public void UnPauseLaser(TypeOfMovement previousMovement)
    {
        typeOfMovement = previousMovement;
    }
}
