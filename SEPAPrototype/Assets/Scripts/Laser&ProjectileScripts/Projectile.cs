using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - 30/4/22
//Edited by Tim Harrison on 12/5/22 - takes faction variable which is passed through to projectile behaviour
//Edited by Tim Harrison on 3/8/22 - Now takes projectileSpeed as argument and sets moveSpeed of projectileBehaviour using this variable
//Edited by Tim Harrison on 9/9/22 - Added parameter for initialDirection and passed it into the setter in projectileBehaviour

//This class just creates and sets up the projectile with all its components and values
public class Projectile
{
    private GameObject projectileObj;
    private ProjectileBehaviour projectileBehaviourS;
    private Rigidbody2D projectileObjRB;
    private SpriteRenderer projectileObjSR;

    public Projectile(Vector2 pos, Vector2 dir, Sprite sprite, string faction, float projectileSpeed, string initialDirection)
    {
        projectileObj = new GameObject();
        projectileObj.name = "projectile";
        projectileObj.layer = 17;
        projectileObj.tag = "Projectile";
        projectileObj.transform.localScale = new Vector2(0.9f, 0.9f);

        projectileBehaviourS = projectileObj.AddComponent<ProjectileBehaviour>();        
        projectileObjRB = projectileObj.AddComponent<Rigidbody2D>();
        projectileObjSR = projectileObj.AddComponent<SpriteRenderer>();
        projectileObj.AddComponent<CircleCollider2D>();

        projectileObjSR.sprite = sprite;
        projectileObjSR.sortingOrder = 6;
        projectileObjRB.gravityScale = 0;
        projectileObjRB.bodyType = RigidbodyType2D.Dynamic;
        projectileBehaviourS.SetPos(pos);
        projectileBehaviourS.SetDir(dir);
        projectileBehaviourS.SetFaction(faction);
        projectileBehaviourS.SetMoveSpeed(projectileSpeed);
        projectileBehaviourS.SetInitialDirection(initialDirection);
    }

    public GameObject GetProjectileObj()
    {
        return projectileObj;
    }
}
