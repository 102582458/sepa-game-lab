using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

//Originally written by Tim Harrison - date unknown
//Edited on 13/8/22 by Tim Harrison - changed script to find laser door by tag (ExitDoor) instead of name
//Edited on 13/8/22 by Tim Harrison - Added variables for changing tiles in tilemap & added call to tile manager to do that
//Edited on 20/8/22 by Tim Harrison - Added enum for tilemaps to pass through to tile manager when setting a tile, uses list of enums now instead of bools
//Edited on 22/8/22 by Tim Harrison - Updated to use new parent script
//Edited on 29/8/22 by Tim Harrison - Added paremeter to ChangeTile method call to be in line with changes in parent
//Edited on 31/8/22 by Nicholas Mueller - Added float check, numOfObjectsOnButton, in the OnTriggerExit2D to stop a bug due to a logic issue.
//Edited on 25/10/22 by Tim Harrison - Added function calls to pause and unpause lasers on enter and exit

public class ButtonBehaviour : ButtonBase
{
    [SerializeField] private Sprite activeSprite;
    [SerializeField] private bool toggle;  
    
    private ButtonBehaviour otherButtonS;

    private float numOfObjectsOnButton = 0f;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        SetUpSound();
        SetUpTileManager();
        //this method returns an object so you need to use an explicit cast to convert it to the object you want
        otherButtonS = (ButtonBehaviour)CheckPairedObject(otherButtonS);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (spriteRenderer.sprite == deactiveSprite || !toggle)
        {
            PlaySound("buttonPress");
        }
        
        spriteRenderer.sprite = activeSprite;

        if (!isDependant)
        {
            SetState();
        }
        else
        {
            colliding = true;
            if (otherButtonS.colliding)
            {
                SetState();
            }
        }

        ChangeTile(otherButtonS);
        numOfObjectsOnButton++;

        //Pause lasers in list of lasers
        foreach (GameObject l in Lasers)
        {
            if (l.TryGetComponent<LaserMovement>(out var lm))
            {
                typeOfMovement.Add(lm.PauseLaser());
            }

        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        numOfObjectsOnButton--;

        if (numOfObjectsOnButton == 0)
        {
            if (!toggle)
            {
                spriteRenderer.sprite = deactiveSprite;

                foreach (GameObject g in SetInactive)
                {
                    g.SetActive(true);
                }
                foreach (GameObject g in SetActive)
                {
                    g.SetActive(false);
                }

                colliding = false;
                completed = false;
            }
        }

        //Unpause lasers in list of lasers
        for (int i = 0; i < Lasers.Count; i++)
        {
            if (Lasers[i].TryGetComponent<LaserMovement>(out var lm))
            {
                lm.UnPauseLaser(typeOfMovement[i]);
                typeOfMovement.RemoveAt(i);
            }            
        }
    }
}
