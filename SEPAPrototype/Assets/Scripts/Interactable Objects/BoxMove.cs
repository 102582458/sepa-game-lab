using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Edited by Tim Harrison on /date unknown/ - Added code to rotate box with direction player is facing
//Edited by Nicholas Mueller on 7/5/22 - Added leniency checks for dropping off objects & picking up
//Edited by Tim Harrison on 8/5/22 - Added reference to sound manager and call to the sound manager when a box is picked up
//Edited by Tim Harrison on 8/5/22 - Added pseudo command manager to check if interacting with projectile shooter and to activate it if so
//Edited by Tim Harrion on 9/5/22 - Added check for mirror object and call to rotate if it is
//Edited by Tim Harrion on 12/5/22 - now pass through faction when calling projectile shooter

public class BoxMove : MonoBehaviour
{
    [SerializeField] private string boxTag;
    [SerializeField] private Transform grabdetect;
    [SerializeField] private Transform boxHolder;
    [SerializeField] private string pickUpButton;
    [SerializeField] private float rayDist;
    private GameObject soundManagerObj;
    private SoundManager soundManager;

    private GameObject grabbedBox = null;
    private RaycastHit2D grabCheck;
    //private bool playerIsMoving = false;
    private Transform newTileMovePoint;

    [SerializeField] private string faction;
    [SerializeField] private LayerMask layerOfObjects;

    [System.NonSerialized] public bool holdingObject = false;
    [System.NonSerialized] public bool isHoldingFireExtinquisher = false;

    private void Start()
    {
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }        
    }

    // Update is called once per frame
    void Update()
    {
        newTileMovePoint = gameObject.GetComponent<PlayerGridMovement>().GetNewTileMovePoint();

        if (Input.GetButtonDown(pickUpButton) && grabbedBox == null)
        {
            RaycastHit2D grabCheck = Physics2D.Raycast(grabdetect.position, transform.right, rayDist, layerOfObjects);

            //Pseudo command manager
            if (grabCheck.collider != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= .20f &&  grabCheck.collider.name == "ProjectileShooter")
            {
                //grabCheck.collider.gameObject.GetComponent<ShootProjectile>().Interact(faction);
            }

            if (grabCheck.collider != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= .20f && grabCheck.collider.name == "Mirror")
            {
                //grabCheck.collider.gameObject.GetComponent<MirrorReflection>().Interact(faction);
            }

            if (grabCheck.collider != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= .20f && (grabCheck.collider.tag == boxTag || grabCheck.collider.tag == "NeutralBox" || grabCheck.collider.tag == "FireExtinquisher"))
            {
                grabbedBox = grabCheck.collider.gameObject;
                holdingObject = true;

                if (grabbedBox.tag == "NeutralBox" || grabbedBox.tag ==  "BlueBox" || grabbedBox.tag == "RedBox")
                {
                    if (soundManager != null)
                    {
                        soundManager.PlaySound("boxPickUp");
                    }                    
                }                

                if (faction == "blue")
                {
                    grabbedBox.layer = LayerMask.NameToLayer("BlueHolding");
                }
                else if (faction == "red")
                {
                    grabbedBox.layer = LayerMask.NameToLayer("RedHolding");
                }
                grabbedBox.transform.position = boxHolder.position;
                grabbedBox.transform.SetParent(transform);

                if (grabbedBox.tag == "FireExtinquisher")
                {
                    isHoldingFireExtinquisher = true;
                }
            }

        }
        else if (Input.GetButtonDown(pickUpButton) && grabbedBox != null)
        {
            if (Vector2.Distance(transform.position, newTileMovePoint.position) <= .20f)
            {
                holdingObject = false;
                grabbedBox.layer = LayerMask.NameToLayer("PickupObjects");
                grabbedBox.transform.SetParent(null);
                grabbedBox.transform.position = new Vector2(Mathf.Round(grabbedBox.transform.position.x), Mathf.Round(grabbedBox.transform.position.y));
                if (grabbedBox.tag == "FireExtinquisher")
                {
                    isHoldingFireExtinquisher = false;
                }
                grabbedBox = null;
            }
        }

        //code to change rotation of held object so that it faces same way as the player
        if (grabbedBox != null)
        {
            grabbedBox.transform.localRotation = Quaternion.identity;
        }

        /*RaycastHit2D grabCheck = Physics2D.Raycast(grabdetect.position, transform.right, rayDist, layerOfObjects);
        //checks if a object is collided and that it is a box
        if(grabCheck.collider != null && playerIsMoving == false && (grabCheck.collider.tag == boxTag || grabCheck.collider.tag == "NeutralBox" || grabCheck.collider.tag == "FireExtinquisher"))
        {
            //checks that pick up button is pressed
            if (Input.GetKeyDown(pickUpButton) && grabbedBox == null )
            {
                grabbedBox = grabCheck.collider.gameObject;
                holdingObject = true;
                if (faction == "blue")
                {
                    grabbedBox.layer = LayerMask.NameToLayer("BlueHolding");
                } else if (faction == "red")
                {
                    grabbedBox.layer = LayerMask.NameToLayer("RedHolding");
                }
                //grabbedBox.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                grabbedBox.transform.position = boxHolder.position;
                grabbedBox.transform.SetParent(transform);

                if (grabbedBox.tag == "FireExtinquisher")
                {
                    isHoldingFireExtinquisher = true;
                }

                //grabCheck.collider.gameObject.transform.parent = boxHolder;
                //grabCheck.collider.gameObject.transform.position = boxHolder.position;
                //grabCheck.collider.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            }
            else if(Input.GetKeyDown(pickUpButton) )
            {
                holdingObject = false;
                grabbedBox.layer = LayerMask.NameToLayer("Foreground");
                //grabbedBox.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                grabbedBox.transform.SetParent(null);
                if (grabbedBox.tag == "FireExtinquisher")
                {
                    isHoldingFireExtinquisher = false;
                }
                grabbedBox = null;
                //grabCheck.collider.gameObject.transform.parent = null;
                //grabCheck.collider.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            }
        }*/
    }
    /*
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, (Vector2) transform.position + Vector2.right * transform.localScale.x * dist);
    }*/
}
