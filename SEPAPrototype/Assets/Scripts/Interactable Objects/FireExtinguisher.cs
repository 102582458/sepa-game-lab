using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Edited by Tim Harrison on 5/5/22 - Added fire extinguisher activating on button press + activation of particle system
//Edited by Tim Harrison on 6/5/22 - Fixed bug on fire extinguisher firing when not held
//Edited by Tim Harrison on 17/5/22 - to be used by command manager and inherit iContinuousUse interface
//Edited by Tim Harrison on 3/8/22 - added counter rotation to parent (removed on 13/8/22)
//Edited by Tim Harrison on 23/9/22 - added extra fields for fireDetects for the new positions where the FE puts out fire and added new raycasts that check in those locations
//Edited by Tim Harrison on 29/9/22 - Destroying fire now sets it tile to burntout in 'firespreading' script
//Edited by Tim Harrison on 18/10/22 - Added sfx for using FE
public class FireExtinguisher : MonoBehaviour, iInteract, iContinuousUse
{
    public float rayDist;
    private bool pickedUp;
    private bool useItem = false;
    private int layerMask;

    [SerializeField] private Transform fireDetect1;
    [SerializeField] private Transform fireDetect2;
    [SerializeField] private Transform fireDetect3;
    private GameObject soundManagerObj;
    private SoundManager soundManager;
    private ParticleSystem extinguisherPS;
    private FireSpreading fireSpreading;

    void Awake()
    {
        GetFE();
        GetFireSpreading();
    }

    private void Start()
    {
        layerMask = LayerMask.GetMask("Fire");

        //Gets reference to sound manager
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        //Calls OnPress() method every update if bool is set (this starts PS and destroys fire objects
        if (useItem)
        {
            PlaySound("fireExtinguisher");
            OnPress();
        }
    }

    //Method defined in interface (iInteract) which is called on player input, picks up and drops this object
    public void Interact(Dictionary<string, object> interactParameters)
    {
        if (!pickedUp)
        {
            //Picks up object and sets layer depending on player
            if ((string)interactParameters["faction"] == "blue")
            {
                gameObject.layer = LayerMask.NameToLayer("BlueHolding");
                AssignParentAndPos((GameObject)interactParameters["player"], (Transform)interactParameters["interactDetect"]);
                PlaySound("boxPickUp");
            }
            else if ((string)interactParameters["faction"] == "red")
            {
                gameObject.layer = LayerMask.NameToLayer("RedHolding");
                AssignParentAndPos((GameObject)interactParameters["player"], (Transform)interactParameters["interactDetect"]);
                PlaySound("boxPickUp");
            }
        }
        else
        {
            //Drops object
            GameObject player = (GameObject)interactParameters["player"];
            player.GetComponent<CommandManager>().SetHoldingObject(false);
            player.GetComponent<CommandManager>().SetHeldItem(null);

            gameObject.layer = LayerMask.NameToLayer("InteractObjects");
            gameObject.transform.SetParent(null);
            gameObject.transform.position = new Vector2(Mathf.Round(gameObject.transform.position.x), Mathf.Round(gameObject.transform.position.y));

            pickedUp = false;
        }
    }

    //Method defined in interface (iContinuousUse) which sets bool to allow fire extinguisher to be called every update
    public void Use()
    {
        useItem = true;
    }

    //Method defined in interface (iContinuousUse) that stops PS and call to fire extinguisher
    public void StopUse()
    {
        soundManager.StopSound(0);
        useItem = false;
        extinguisherPS.Stop();
    }

    //Assigns parent and position of object, called when object picked up
    private void AssignParentAndPos(GameObject player, Transform playerChildTransform)
    {
        gameObject.transform.SetParent(player.transform);
        //gameObject.transform.position = Vector2.left * transform.localScale/* * 0.5f*/;

        player.GetComponent<CommandManager>().SetHoldingObject(true);
        player.GetComponent<CommandManager>().SetHeldItem(gameObject);

        gameObject.transform.position = playerChildTransform.position;
        //gameObject.GetComponentInChildren<SpriteRenderer>().transform.position = new Vector2(gameObject.transform.position.x , gameObject.transform.position.y+0.5f);
        gameObject.transform.localRotation = Quaternion.identity;

        pickedUp = true;
    }

    //Called when object is used, starts PS and destroys fire
    private void OnPress()
    {
        //Create list of raycsasts
        List<RaycastHit2D> fireChecks = new List<RaycastHit2D>();
        

        //Raycasts check if there is something in the 'Fire' layer at their location
        fireChecks.Add(Physics2D.Raycast(gameObject.transform.position, new Vector2(0, 0), rayDist, layerMask));
        fireChecks.Add(Physics2D.Raycast(fireDetect1.position, Vector2.right * transform.localPosition, rayDist, layerMask));
        fireChecks.Add(Physics2D.Raycast(fireDetect2.position, Vector2.right * transform.localPosition, rayDist, layerMask));
        fireChecks.Add(Physics2D.Raycast(fireDetect3.position, Vector2.right * transform.localPosition, rayDist, layerMask));

        //Checks if the particle effect is playing, if not, plays it
        if (!extinguisherPS.isPlaying)
        {
            extinguisherPS.Play();
        }

        foreach (RaycastHit2D r in fireChecks)
        {
            //Debug.Log($""/* {r.transform.gameObject}"*/);
            if (r.collider != null && r.collider.CompareTag("Fire"))
            {
                //Sets cell as burntout in 'firespreading' when it is destroyed by this
                if (fireSpreading != null)
                {
                    fireSpreading.SetBurntOutTile((int)r.collider.transform.position.x, (int)r.collider.transform.position.y);
                }                
                Destroy(r.transform.gameObject);
            }
        }

        //if (fireCheck.collider != null && fireCheck.collider.tag == "Fire")
        //{
        //    Destroy(fireCheck.transform.gameObject);
        //}
    }

    //Manages calls to sound manager
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }        
    }

    private void OnEnable()
    {
        GetFE();
        GetFireSpreading();
    }

    private void GetFE()
    {
        //Gets reference to extinguisher particle system and turns it off
        extinguisherPS = gameObject.GetComponentInChildren<ParticleSystem>();
        extinguisherPS.Stop();
    }

    private void GetFireSpreading()
    {
        if(GameObject.FindGameObjectWithTag("FireSpreading") != null)
        {
            fireSpreading = GameObject.FindGameObjectWithTag("FireSpreading").GetComponent<FireSpreading>();
        }        
    }
}
