using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - Sem1 2022
//Edited on 3/8/2022 by Tim Harrison - now uses forloop and ignores collision between projectiles and zones

public class IgnoreCollisionNeutral : MonoBehaviour
{
    private int[] ignoredLayers = { 18, 17 };

    // Start is called before the first frame update
    void Awake()
    {
        foreach (int i in ignoredLayers)
        {
            Physics2D.IgnoreLayerCollision(i, 11);
            Physics2D.IgnoreLayerCollision(i, 17);
        }
    }
}
