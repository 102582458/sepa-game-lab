using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 17/5/22
//Edited by Tim Harrison on 3/8/22 - added counter rotation to parent
//Edited by Tim Harrison on 20/8/22 - plays sounds when box is placed
//Edited by Tim Harrison on 18/10/22 - add sfx for cant interact

public class Crate : MonoBehaviour, iInteract
{
    [SerializeField] private string faction;

    private bool pickedUp;
    private GameObject soundManagerObj;
    private SoundManager soundManager;

    void Start()
    {
        //Sets up reference to sound manager
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }
    }

    void Update()
    {
        //Rotates object in opposite direction of parent
        if (pickedUp)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);
        }
    }

    //Method defined in interface which is called on player input, picks up and drops this object
    public void Interact(Dictionary<string, object> interactParameters)
    {
        if (!pickedUp)
        {
            //Assigns new layer parent and position based on which players picks it up
            if ((string)interactParameters["faction"] == faction)
            {
                AssignLayer((string)interactParameters["faction"]);
                AssignParentAndPos((GameObject)interactParameters["player"], (Transform)interactParameters["interactDetect"]);
                PlaySound("boxPickUp");
            }
            else if (faction == "neutral")
            {
                AssignLayer((string)interactParameters["faction"]);
                AssignParentAndPos((GameObject)interactParameters["player"], (Transform)interactParameters["interactDetect"]);
                PlaySound("boxPickUp");
            }
            else
            {
                PlaySound("cannotInteract");
            }
        }
        else
        {
            //Drops object
            GameObject player = (GameObject)interactParameters["player"];
            player.GetComponent<CommandManager>().SetHoldingObject(false);
            player.GetComponent<CommandManager>().SetHeldItem(null);

            gameObject.layer = LayerMask.NameToLayer("InteractObjects");
            gameObject.transform.SetParent(null);
            gameObject.transform.position = new Vector2(Mathf.Round(gameObject.transform.position.x), Mathf.Round(gameObject.transform.position.y));

            pickedUp = false;

            PlaySound("placeBox");
        }
    }

    //Assigns new layer to crate when picked up
    public void AssignLayer(string playerFaction)
    {
        if (playerFaction == "blue")
        {
            gameObject.layer = LayerMask.NameToLayer("BlueHolding");
        }
        else if (playerFaction == "red")
        {
            gameObject.layer = LayerMask.NameToLayer("RedHolding");
        }
    }

    //Assigns new parent and position when picked up
    public void AssignParentAndPos(GameObject player, Transform playerChildTransform)
    {
        gameObject.transform.SetParent(player.transform);

        player.GetComponent<CommandManager>().SetHoldingObject(true);
        player.GetComponent<CommandManager>().SetHeldItem(gameObject);

        gameObject.transform.position = playerChildTransform.position;
        gameObject.transform.localRotation = Quaternion.identity;

        pickedUp = true;
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }
}
