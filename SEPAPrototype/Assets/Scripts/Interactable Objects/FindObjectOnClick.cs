using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Made by Conor on the 05/09/22: finds the gameobject to turn on or off
public class FindObjectOnClick : MonoBehaviour
{
    [SerializeField] private string findThis;
    private GameObject foundObject;
    DialogueOff DialogueOff;
    private static bool On = true;
    private GameObject dialogueOn;
    [SerializeField] private GameObject button1;
    // Start is called before the first frame update
    void Awake()
    {
        // looks for the named game object
        foundObject = GameObject.Find(findThis);
        DialogueOff = foundObject.GetComponent<DialogueOff>();
        

        //changes the icon for the toggle depending on the state of the game object
        if (On == true)
        {
            button1.GetComponent<Toggle>().SetIsOnWithoutNotify(true);
        }
        else
        {
            button1.GetComponent<Toggle>().SetIsOnWithoutNotify(false);
        }

    }

    
    //togles gameobject on or off and sets the bool to reflect the state of the game object
    public void EnableObject()
    {
        DialogueOff.DialougOff();
        On = !On;
    }
}
