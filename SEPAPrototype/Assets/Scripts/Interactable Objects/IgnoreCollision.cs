using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison - Sem1 2022
//Edited on 3/8/2022 by Tim Harrison - now uses forloop and ignores collision between projectiles and zones
//Edited on 25/9/22 by Tim Harrison - Players now found via script on awake and enable
//Edited on 26/9/22 by Tim Harrison - Moved ignore objects code into a function

public class IgnoreCollision : MonoBehaviour
{
    private enum Faction
    {
        red,
        blue,
    }

    private GameObject sameColourPlayer;

    private int[] ignoredLayers = { 12, 13, 17 };

    [SerializeField] private Faction faction;
    private GameObject[] players;

    // Start is called before the first frame update
    void Awake()
    {
        IgnorePlayers();

        IgnoreObjects();
    }

    //Call when object is set to active
    private void OnEnable()
    {
        IgnorePlayers();

        IgnoreObjects();
    }

    //Ignores objects in relevant layers
    private void IgnoreObjects()
    {
        foreach (int i in ignoredLayers)
        {
            Physics2D.IgnoreLayerCollision(i, 11);
            Physics2D.IgnoreLayerCollision(i, 17);
        }
    }

    //Ignores player of same faction
    private void IgnorePlayers()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject g in players)
        {
            if (g.GetComponent<CommandManager>().GetFaction() == faction.ToString())
            {
                sameColourPlayer = g;
                Physics2D.IgnoreCollision(sameColourPlayer.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            }
        }
    }
}
