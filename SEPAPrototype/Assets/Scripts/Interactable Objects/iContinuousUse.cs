    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 17/5/22

//Interface used by all continuous use objects
interface iContinuousUse
{
    public void Use();
    public void StopUse();
}
