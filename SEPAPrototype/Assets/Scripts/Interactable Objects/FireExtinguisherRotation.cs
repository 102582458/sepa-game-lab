using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Originally written by Tim Harrison on 13/08/22

public class FireExtinguisherRotation : MonoBehaviour
{
    private bool previousDirectionLeft;
    
    // Start is called before the first frame update
    void Start()
    {
        if (transform.rotation == Quaternion.Euler(0f, 180f, 0f))
        {
            previousDirectionLeft = true;
        }
        else
        {
            previousDirectionLeft = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if parent has parent (ie if the player has picked it up)
        if (transform.parent.parent != null)
        {
            //counters rotatoin of parent's parent's rotation
            transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);

            //Checks if player is facing left and flips sprite if so (remembers if player was facing left and so sprite will keep facing left till player turns right)
            if (transform.parent.parent.rotation == Quaternion.Euler(0f, 0f, -180f) || previousDirectionLeft)
            {
                transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                previousDirectionLeft = true;
            }
            
            if (transform.parent.parent.rotation == Quaternion.Euler(0f, 0f, 0f))
            {
                previousDirectionLeft = false;
            }
        }        
    }
}
