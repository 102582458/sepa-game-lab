using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

//Originally written by Tim Harrison - 22/8/22
//Edited on 29/8/22 by Tim Harrison - added check to see if object is dependant for tile updating
//Edited by Tim Harrison on 25/10/22 - Added a pair of lists for taking in lasers and storing their state

public abstract class ButtonBase : MonoBehaviour
{
    private enum Maps
    {
        Ground,
        UpperGround,
        Walls,
        UpperWalls
    }

    [SerializeField] protected GameObject[] pairedObject;
    [SerializeField] protected List<GameObject> SetActive;
    [SerializeField] protected List<GameObject> SetInactive;
    [SerializeField] protected bool isDependant;

    //Variables for changing tile in tilemap
    [SerializeField] private bool willChangeTile;
    [SerializeField] private List<Vector3Int> tilePositions;
    [SerializeField] private List<Tile> tiles;
    [SerializeField] private List<Maps> map;

    [SerializeField] protected Sprite deactiveSprite;

    [SerializeField] protected List<GameObject> Lasers;
    protected List<LaserMovement.TypeOfMovement> typeOfMovement = new();

    private GameObject soundManagerObj;
    protected SoundManager soundManager;

    private GameObject tileManagerObj;
    protected TileManager tileManager;

    protected SpriteRenderer spriteRenderer;
    protected bool completed = false;
    protected bool colliding = false;

    //code that changes a tile on the tile map according to the set parameters
    protected void ChangeTile(ButtonBase otherTrigger)
    {
        if (willChangeTile)
        {
            if (!isDependant)
            {
                ChangeTileExecute();
            }
            else
            {
                colliding = true;
                if (otherTrigger.colliding)
                {
                    ChangeTileExecute();
                }
            }            
        }
    }

    private void ChangeTileExecute()
    {
        //Makes sure lists are same length
        if (tiles.Count == map.Count && tiles.Count == tilePositions.Count)
        {
            //Call tilemanager for each entry in lists
            int i = 0;
            foreach (Tile t in tiles)
            {
                tileManager.UpdateTile(t, map[i].ToString(), tilePositions[i]);
                i++;
            }
        }
        else
        {
            Debug.Log($"ERROR: UNEQUAL NUMBER OF VALUES FOR TILEPOSISTIONS {tilePositions.Count}, TILES {tiles.Count} AND MAP {map.Count}, ALL LISTS MUST BE THE SAME LENGHT");
        }
    }

    //code that checks if there is a paired object, this should be overriden in all scripts except 'buttonbehaviour'
    protected virtual object CheckPairedObject(Object otherObjectS)
    {
        if (pairedObject.Length == 1)
        {
            //replace the component that is being got with the relevant script when overriding (eg. in projectilereceiver script getcomponent<projectilereceiver>()
            otherObjectS = pairedObject[0].GetComponent<ButtonBehaviour>();
            return otherObjectS;
        }
        else
        {
            return null;
        }
    }

    protected void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    //sets all objects to inactive or active depending on the list theyre in and plays all relevant sfx
    protected void SetState()
    {
        if (!completed)
        {
            foreach (GameObject g in SetInactive)
            {
                if (g.tag == "ExitDoor")
                {
                    PlaySound("gateOpening");
                }
                g.SetActive(false);
            }

            foreach (GameObject g in SetActive)
            {
                g.SetActive(true);
            }

            completed = true;
        }
    }

    //fetches sound manager from scene
    protected void SetUpSound()
    {
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }
    }

    //fetches tile manager from scene
    protected void SetUpTileManager()
    {
        tileManagerObj = GameObject.FindGameObjectWithTag("TileManager");
        if (tileManagerObj != null)
        {
            tileManager = tileManagerObj.GetComponent<TileManager>();
        }
    }

    public bool GetCompleted()
    {
        return completed;
    }
}
