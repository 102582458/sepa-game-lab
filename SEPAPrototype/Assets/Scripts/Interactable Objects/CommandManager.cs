using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.InputSystem;

//Written by Tim Harrison on 17/5/22
//Edited by Nicholas Mueller on 22/5/22 - Updated script to use the new input system, now gets player input from the PlayerInputController script
//Edited by Nicholas Mueller on 21/09/2022 - Added function to force drop items when the players die
//Edited by Tim Harrison on 18/10/22 - Added sfx for cant interact (although this call is a catch and should never actually play, actual sfx calls are in object scripts)

public class CommandManager : MonoBehaviour
{
    //[SerializeField] private string interactButton;
    //[SerializeField] private string useButton;
    private PlayerControls playerControls;
    [SerializeField] private string faction;
    //[SerializeField] private float rayDist;
    [SerializeField] private Transform interactDetect; //change this so it finds dynamically with code
    [SerializeField] private LayerMask interactableObjectsLayer;

    private float leniencyDist = 0.2f;
    private bool holdingObject;
    private bool isContinuousUseObject;
    private Transform newTileMovePoint;
    private GameObject heldItem = null;
    private GameObject itemBeingUsed = null;
    private Collider2D interactCheck;
    private iInteract interaction;
    private iUse use;
    private iContinuousUse continuousUse;

    private int allGamepads, currentGamepads;

    private Dictionary<string, object> interactParameters = new();

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    // Start is called before the first frame update
    void Awake()
    {
        //Adds all objects to parameter dictionary
        interactParameters.Add("faction", faction);
        interactParameters.Add("interactDetect", interactDetect);
        interactParameters.Add("player", gameObject);
        interactParameters.Add("originalLayer", interactableObjectsLayer);
        //Sets the player controls for the start of the game
        SetControls();
    }

    private void Start()
    {
        //sound manager setup
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }
    }

    //Enables the player controls
    private void OnEnable()
    {
        playerControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        playerControls.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        //Gets called to check if change in player input
        CheckInput();
        //Gets transform from player movement
        newTileMovePoint = gameObject.GetComponent<PlayerGridMovement>().GetNewTileMovePoint();

        //Checks input, holding object, leniency distance & if the target object has the iInteract interface, if it does calls the associated method
        /*if (Input.GetButtonDown(interactButton) && holdingObject == false)
        {
            //interactCheck = Physics2D.Raycast(interactDetect.position, transform.right, rayDist, interactableObjectsLayer);
            interactCheck = Physics2D.OverlapBox(interactDetect.position, new Vector2(0.95f, 0.95f), 0f, interactableObjectsLayer);

            if (interactCheck != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            {
                interaction = interactCheck.GetComponent<iInteract>();
                if (interaction != null)
                {
                    interaction.Interact(interactParameters);
                }
            }
        } //Checks if input and holding object, calls the interface method again (a bool and an if else statement will need to be set in any object using this that determines what is done here
        else if (Input.GetButtonDown(interactButton) && holdingObject == true)
        {
            if (Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            {
                heldItem.GetComponent<iInteract>().Interact(interactParameters);
            }            
        }*/

    }

    private void Interact(InputAction.CallbackContext context)
    {
        //Checks input, holding object, leniency distance & if the target object has the iInteract interface, if it does calls the associated method
        if (holdingObject == false)
        {
            //interactCheck = Physics2D.Raycast(interactDetect.position, transform.right, rayDist, interactableObjectsLayer);
            interactCheck = Physics2D.OverlapBox(interactDetect.position, new Vector2(0.95f, 0.95f), 0f, interactableObjectsLayer);

            if (interactCheck != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            {
                interaction = interactCheck.GetComponent<iInteract>();
                if (interaction != null)
                {
                    interaction.Interact(interactParameters);
                }
                else
                {
                    PlaySound("cannotInteract");
                }
            }
            
        } 
        //Checks if input and holding object, calls the interface method again (a bool and an if else statement will need to be set in any object using this that determines what is done here
        else if (holdingObject == true)
        {
            if (Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            {
                heldItem.GetComponent<iInteract>().Interact(interactParameters);
            }
        }
    }

    private void Use(InputAction.CallbackContext context)
    {

        if (holdingObject == false)
        {
            //interactCheck = Physics2D.Raycast(interactDetect.position, transform.right, rayDist, interactableObjectsLayer);

            //if (interactCheck.collider != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            //{
            //    itemBeingUsed = interactCheck.collider.gameObject;
            //    use = interactCheck.collider.GetComponent<iUse>();
            //    continuousUse = itemBeingUsed.GetComponent<iContinuousUse>();

            //    SelectUse();
            //}
        }
        else if (holdingObject == true)
        {
            itemBeingUsed = heldItem;
            use = itemBeingUsed.GetComponent<iUse>();
            continuousUse = itemBeingUsed.GetComponent<iContinuousUse>();

            SelectUse();
        }
    }

    private void StopUse(InputAction.CallbackContext context)
    {
        //Checks if a continuous use object is being used and if the input is released calls to stop using that object
        if (isContinuousUseObject)
        {
            itemBeingUsed.GetComponent<iContinuousUse>().StopUse();
            isContinuousUseObject = false;
        }
    }

    //Determines whether an object uses the iUse or iContinuousUse interface and calls the appropriate method
    private void SelectUse()
    {
        if (use != null)
        {
            use.Use();
        }
        else if (continuousUse != null)
        {
            continuousUse.Use();
            isContinuousUseObject = true;
        }
    }

    //Getters and setters for various variables
    public bool GetHoldingObject()
    {
        return holdingObject;
    }

    public string GetFaction()
    {
        return faction;
    }

    public void SetHoldingObject(bool b)
    {
        holdingObject = b;
    }

    public void SetHeldItem(GameObject gameobject)
    {
        heldItem = gameobject;
    }

    public GameObject GetHeldItem()
    {
        return heldItem;
    }

    public Dictionary<string, object> GetInteractParameters()
    {
        return interactParameters;
    }

    //Gets called to update player controls
    private void SetControls()
    {
        if (faction == "blue")
        {
            playerControls = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>().GetBlueCharacterControls();
            playerControls.PlayerBlue.InteractButton.performed += Interact;
            playerControls.PlayerBlue.UseButton.performed += Use;
            playerControls.PlayerBlue.UseButton.canceled += StopUse;
        }
        else if (faction == "red")
        {
            playerControls = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>().GetRedCharacterControls();
            playerControls.PlayerRed.InteractButton.performed += Interact;
            playerControls.PlayerRed.UseButton.performed += Use;
            playerControls.PlayerRed.UseButton.canceled += StopUse;
        }
    }

    //Checks if the number of gamepads have changed
    private void CheckInput()
    {
        allGamepads = Gamepad.all.Count;
        if (allGamepads != currentGamepads)
        {
            currentGamepads = allGamepads;
            SetControls();
        }
    }

    public void ForceDropItem()
    {
        if (holdingObject == true)
        {
            heldItem.GetComponent<iInteract>().Interact(interactParameters);
        }
    }
    
    private void OnDestroy()
    {
        if (faction == "blue")
        {
            playerControls.PlayerBlue.InteractButton.performed -= Interact;
            playerControls.PlayerBlue.UseButton.performed -= Use;
            playerControls.PlayerBlue.UseButton.canceled -= StopUse;
        }
        else if (faction == "red")
        {
            playerControls.PlayerRed.InteractButton.performed -= Interact;
            playerControls.PlayerRed.UseButton.performed -= Use;
            playerControls.PlayerRed.UseButton.canceled -= StopUse;
        }
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }
}
