using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 17/5/22

//Interface used by all interactable objects
interface iInteract
{
    public void Interact(Dictionary<string, object> interactParameters);
}
