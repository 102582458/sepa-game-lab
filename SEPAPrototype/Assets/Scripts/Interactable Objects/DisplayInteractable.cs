using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//Made by Conor on the 12/09/22: to display the contoles for interacting with objects
//edited by conor on the 21/09: made the code work by using OnTriggerStay2D
//edited by Conor on the 30/09: added redandence so it will disable every button then not looking at something, added check that an object is not being held to stop flickering when player moves
//edited by conor on the 01/10: mad it so that the raycast ignores the other players holding layer

public class DisplayInteractable : MonoBehaviour
{
    /*
    [SerializeField] private Transform interactDetect; //change this so it finds dynamically with code
    //private RaycastHit2D interactableCheck;
    private Collider2D interactCheck;
   
    private Transform newTileMovePoint;
    [SerializeField] private LayerMask interactableObjectsLayer;
    private float leniencyDist = 0.2f;
    */
    [SerializeField] private GameObject Player;
    private bool holdingObject = false;
    //button sprites
    [SerializeField] private GameObject ButtonPrompt1Controller;
    [SerializeField] private GameObject ButtonPrompt2Controller;
    [SerializeField] private GameObject ButtonPrompt1KeyBoard;
    [SerializeField] private GameObject ButtonPrompt2KeyBoard;
    //player variables
    [SerializeField] private bool RedPlayer = false;
    [SerializeField] private bool BluePlayer = false;
    private PlayerInputController PIC;
    private int BluePlayerInput;
    private int RedPlayerInput;
    private LayerMask Ignorelayer;
    
    // Start is called before the first frame update
    void Start()
    {
        PIC = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>();
        //sets the holding layer for other player to be ignored
        if (RedPlayer == true)
        {
            Ignorelayer = LayerMask.GetMask("BlueHolding");
        }
        if (BluePlayer == true)
        {
            Ignorelayer = LayerMask.GetMask("RedHolding");
        }
    }
   
        // Update is called once per frame
    void Update()
    {
        holdingObject = Player.GetComponent<CommandManager>().GetHoldingObject();
        //stops promps from rotating 
        ButtonPrompt1Controller.transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);
        ButtonPrompt2Controller.transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);
        ButtonPrompt1KeyBoard.transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);
        ButtonPrompt2KeyBoard.transform.rotation = Quaternion.Euler(0f, 0f, transform.parent.rotation.z * -1);

        //gets the players input controles 
        BluePlayerInput = PIC.GetPlayer1Input();
        RedPlayerInput = PIC.GetPlayer2Input();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 1.0f, ~Ignorelayer);

        if (hit.collider != null)
        {
            
            
            //red player inputs
            if (RedPlayer == true)
            {
                //checks if the player is looking at an interactable object
                if (hit.collider.tag == "RedBox" || hit.collider.tag == "FireExtinquisher" || hit.collider.tag == "NeutralBox" || hit.collider.tag == "Box" || hit.collider.tag == "Mirror" || hit.collider.tag == "ProjectileShooter" )
                {
                   
                    if(RedPlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(true);

                        //if players change controllers while looking at an object deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);
                    }
                    else if(RedPlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(true);

                        //if players change controllers while looking at an object deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                    }
                        

                }
                else if (holdingObject == false)
                {
                    ButtonPrompt1Controller.SetActive(false);
                    ButtonPrompt1KeyBoard.SetActive(false);
                    ButtonPrompt2Controller.SetActive(false);
                    ButtonPrompt2KeyBoard.SetActive(false);
                }
            }
            //blue player inputs
            if (BluePlayer == true)
            {
                //checks if the player is looking at an interactable object
                if (hit.collider.tag == "BlueBox" || hit.collider.tag == "FireExtinquisher" || hit.collider.tag == "NeutralBox" || hit.collider.tag == "Box" || hit.collider.tag == "Mirror" || hit.collider.tag == "ProjectileShooter")
                {
                    if(BluePlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(true);

                        //if players change controllers while looking at an object deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);

                    }
                    else if (BluePlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(true);

                        //if players change controllers while looking at an object deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                    }


                }
                else if (holdingObject == false)
                {
                    ButtonPrompt1Controller.SetActive(false);
                    ButtonPrompt1KeyBoard.SetActive(false);
                    ButtonPrompt2Controller.SetActive(false);
                    ButtonPrompt2KeyBoard.SetActive(false);
                }
            }

            //checks if the player is holding the fire extinguisher
            if (hit.transform.tag == "FireExtinquisher" && holdingObject == true)
            {
                //blue player inputs
                if (BluePlayer == true)
                {
                    if (BluePlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(true);

                        //if players change controllers while holding an object deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(false);


                    }
                    else if (BluePlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(true);

                        //if players change controllers while holding an object deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(false);
                    }
                    
                }
                //red player inputs
                if (RedPlayer == true)
                {
                    if (RedPlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(true);

                        //if players change controllers while holding an object deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(false);

                    }
                    else if (RedPlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(true);

                        //if players change controllers while holding an object deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(false);
                    }

                }
            }
            else if (hit.transform.tag == "FireExtinquisher" && holdingObject == false)
            {
                //red player inputs
                if (RedPlayer == true)
                {
                    if (RedPlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(true);
                        ButtonPrompt2Controller.SetActive(false);

                        //if players change controllers deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(false);

                    }
                    else if (RedPlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(true);
                        ButtonPrompt2KeyBoard.SetActive(false);

                        //if players change controllers deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(false);
                    }
                }
                //blue player inputs
                if (BluePlayer == true)
                {
                    if (BluePlayerInput == 1)
                    {
                        ButtonPrompt1Controller.SetActive(true);
                        ButtonPrompt2Controller.SetActive(false);
                        //if players change controllers deactivates the old sprites
                        ButtonPrompt1KeyBoard.SetActive(false);
                        ButtonPrompt2KeyBoard.SetActive(false);

                    }
                    else if (BluePlayerInput == 0)
                    {
                        ButtonPrompt1KeyBoard.SetActive(true);
                        ButtonPrompt2KeyBoard.SetActive(false);
                        //if players change controllers deactivates the old sprites
                        ButtonPrompt1Controller.SetActive(false);
                        ButtonPrompt2Controller.SetActive(false);
                    }
                }
            }
           

        }
        else if (hit.collider == null)
        {
            ButtonPrompt1Controller.SetActive(false);
            ButtonPrompt1KeyBoard.SetActive(false);
            ButtonPrompt2Controller.SetActive(false);
            ButtonPrompt2KeyBoard.SetActive(false);
        }
        /*newTileMovePoint = gameObject.GetComponent<PlayerGridMovement>().GetNewTileMovePoint();

        if (holdingObject == false)
        {
            //interactCheck = Physics2D.Raycast(interactDetect.position, transform.right, rayDist, interactableObjectsLayer);
            interactCheck = Physics2D.OverlapBox(interactDetect.position, new Vector2(0.95f, 0.95f), 0f, interactableObjectsLayer);

            if (interactCheck != null && Vector2.Distance(transform.position, newTileMovePoint.position) <= leniencyDist)
            {
                ButtonPrompt.SetActive(true);
               
            }
            else
            {
                ButtonPrompt.SetActive(false);
            }
        }
        */

    }
 /*
    private void OnTriggerStay2D(Collider2D collision)
    {
        
        //checks if the player is looking at an interactable object
        if (RedPlayer = true)
        {
            if(collision.gameObject.tag == "RedBox" || collision.gameObject.tag == "FireExtinquisher" || collision.gameObject.tag == "NeutralBox" || collision.gameObject.tag == "Box" || collision.gameObject.tag == "Mirror" || collision.gameObject.tag == "Projectile")
            {
                ButtonPrompt.SetActive(true);
            
            }
        }
        if (BluePlayer = true)
        {
            if (collision.gameObject.tag == "BlueBox" || collision.gameObject.tag == "FireExtinquisher" || collision.gameObject.tag == "NeutralBox" || collision.gameObject.tag == "Box" || collision.gameObject.tag == "Mirror" || collision.gameObject.tag == "ProjectileShooter")
            {
                ButtonPrompt.SetActive(true);

            }
        }
        
        //checks if the player is holding the fire extinguisher
        if(collision.gameObject.tag == "FireExtinquisher" && holdingObject == true)
        {
            ButtonPrompt.SetActive(false);
            ButtonPrompt2.SetActive(true);
        }
        else if(collision.gameObject.tag == "FireExtinquisher" && holdingObject == false)
        {
            ButtonPrompt.SetActive(true);
            ButtonPrompt2.SetActive(false);
        }

       //Debug.Log("hit");
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        ButtonPrompt.SetActive(false);
        ButtonPrompt2.SetActive(false);
    }
   */
}
