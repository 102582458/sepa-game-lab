using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 17/5/22

//Is an interface use by all objects with a "use" function (not continuous use)
interface iUse
{
    public void Use();
}
