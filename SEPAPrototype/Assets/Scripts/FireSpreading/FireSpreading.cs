using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Tilemaps;

//Originally written by Tim Harrison - 6/9/22
//Edited by Tim Harrison on 29/9/22 - Added public method that can be called to set cells to burntout
//Edited by Tim Harrison on 2/10/22 - Fire won't spread if there is a dialogue manager

public class FireSpreading : MonoBehaviour
{
    private enum Status
    {
        noFire,
        Fire,
        BurnOut,
    }

    [SerializeField] private GameObject fire;
    [SerializeField] private Tilemap map;

    [SerializeField] private float waitTime;
    [SerializeField] private float spreadChance;
    [SerializeField] private bool burnOut;

    private int Rows;
    private int Columns;

    private Status[,] grid = new Status[0, 0];
    private Status[,] nextGeneration = new Status[0, 0];

    // Start is called before the first frame update
    void Start()
    {
        Rows = map.size.x;
        Columns = map.size.y;

        grid = new Status[Rows, Columns];
        nextGeneration = new Status[Rows, Columns];
        
        //Set all values in grid to have noFire
        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                grid[row, column] = Status.noFire;
            }
        }

        GameObject[] fireList = GameObject.FindGameObjectsWithTag("Fire");
        foreach (GameObject g in fireList)
        {
            grid[(int)g.transform.position.x - map.cellBounds.position.x, (int)g.transform.position.y - map.cellBounds.position.y] = Status.Fire;
        }

        StartCoroutine(FireSpreadSpeed(waitTime));
    }

    private IEnumerator FireSpreadSpeed (float waitTime)
    {
        //Destroy all fire in the scene
        GameObject[] fireList = GameObject.FindGameObjectsWithTag("Fire");
        foreach (GameObject g in fireList)
        {
            Destroy(g);
        }

        //Instantiate fire in every cell that is set to Fire
        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                var cell = grid[row, column];
                if (cell == Status.Fire)
                {
                    Instantiate(fire, new Vector3(row + map.cellBounds.position.x, column + map.cellBounds.position.y, 0), Quaternion.identity);
                }
            }
        }        

        yield return new WaitForSeconds(waitTime);

        if (FindObjectOfType<DialogueManager>() == null)
        {
            //Get grid values for next loop
            grid = NextGeneration(grid);
        }

        StartCoroutine(FireSpreadSpeed(waitTime));
    }

    private Status[,] NextGeneration(Status[,] currentGrid)
    {
        Status[,] nextGeneration = new Status[Rows, Columns];

        //Loop through every cell
        for (int row = 1; row < Rows - 1; row++)
        {
            for (int column = 1; column < Columns - 1; column++)
            {
                //Find neighbours that are on fire
                var aliveNeighbours = 0;
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        aliveNeighbours += currentGrid[row + i, column + j] == Status.Fire ? 1 : 0;
                    }
                }

                //Remove cell from list of its neighbours and remove diagonal cells
                var currentCell = currentGrid[row, column];
                aliveNeighbours -= currentCell == Status.Fire ? 1 : 0;
                aliveNeighbours -= currentGrid[row - 1, column - 1] == Status.Fire ? 1 : 0;
                aliveNeighbours -= currentGrid[row - 1, column + 1] == Status.Fire ? 1 : 0;
                aliveNeighbours -= currentGrid[row + 1, column - 1] == Status.Fire ? 1 : 0;
                aliveNeighbours -= currentGrid[row + 1, column + 1] == Status.Fire ? 1 : 0;

                

                //If there is one nieghbour on fire, has a chance to catch on fire
                if (currentCell == Status.noFire && aliveNeighbours > 0)
                {
                    if (map.GetTile(new Vector3Int(row + map.cellBounds.position.x, column + map.cellBounds.position.y, 0)))
                    {
                        float rng = Random.Range(0f, 1f);
                        if (rng < spreadChance)
                        {
                            nextGeneration[row, column] = Status.Fire;
                        }
                    }                                      
                }
                //If completely surrond, 'burns out'
                else if (currentCell == Status.Fire && aliveNeighbours == 4 && burnOut)
                {
                    nextGeneration[row, column] = Status.BurnOut;
                }
                else if (currentCell == Status.Fire && aliveNeighbours < 1 && burnOut)
                {
                    nextGeneration[row, column] = Status.BurnOut;
                }
                //Stays the same
                else
                {
                    nextGeneration[row, column] = currentCell;
                }                
            }
        }

        return nextGeneration;
    }

    //Sets cells in grid as burntout
    public void SetBurntOutTile(int row, int column)
    {
        if (burnOut)
        {
            grid[row - map.cellBounds.position.x, column - map.cellBounds.position.y] = Status.BurnOut;
        }
    }
}
