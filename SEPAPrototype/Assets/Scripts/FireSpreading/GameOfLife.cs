using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Tilemaps;

//Originally written by Tim Harrison on 6/8/22

public class GameOfLife : MonoBehaviour
{
    private enum Status
    {
        noFire,
        Fire,
    }

    [SerializeField] private GameObject fire;
    [SerializeField] private Tilemap map;

    private float waitTime = 1f;

    private int Rows;
    private int Columns;

    private Status[,] grid = new Status[0, 0];
    private Status[,] nextGeneration = new Status[0, 0];

    // Start is called before the first frame update
    void Start()
    {
        Rows = map.size.x;
        Columns = map.size.y;

        grid = new Status[Rows, Columns];
        nextGeneration = new Status[Rows, Columns];

        //Randomly initialise grid
        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                grid[row, column] = (Status)Random.Range(0, 2);
            }            
        }

        StartCoroutine(FireSpreadSpeed(waitTime));
    }

    private IEnumerator FireSpreadSpeed (float waitTime)
    {
        //Loop through every cell
        for (int row = 1; row < Rows - 1; row++)
        {
            for (int column = 1; column < Columns - 1; column++)
            {
                //Find alive neighbours
                var aliveNeighbours = 0;
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        aliveNeighbours += grid[row + i, column + j] == Status.Fire ? 1 : 0;
                    }
                }

                var currentCell = grid[row, column];
                //Remove cell from list of its neighbours
                aliveNeighbours -= currentCell == Status.Fire ? 1 : 0;

                //Cell is lonely and dies
                if (currentCell == Status.Fire && aliveNeighbours < 2)
                {
                    nextGeneration[row, column] = Status.noFire;
                }

                //Cell dies due to over population
                else if (currentCell == Status.Fire && aliveNeighbours > 3)
                {
                    nextGeneration[row, column] = Status.noFire;
                }

                //A new cell is born
                else if (currentCell == Status.noFire && aliveNeighbours == 3)
                {
                    nextGeneration[row, column] = Status.Fire;
                }

                //Stays the same
                else
                {
                    nextGeneration[row, column] = currentCell;
                }
            }
        }

        //Loops through every cell and creates fire for each one that is set to 'Fire'
        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                var cell = grid[row, column];
                if (cell == Status.Fire)
                {
                    Instantiate(fire, new Vector3(row + map.cellBounds.position.x, column + map.cellBounds.position.y, 0), Quaternion.identity);
                }
            }
        }

        //Sets up array of tiles for next loop
        grid = nextGeneration;


        yield return new WaitForSeconds(waitTime);

        //Clears Fires
        GameObject[] fireList = GameObject.FindGameObjectsWithTag("Fire");
        foreach (GameObject g in fireList)
        {
            Destroy(g);
        }

        StartCoroutine(FireSpreadSpeed(waitTime));
    }
}
