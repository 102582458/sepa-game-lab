using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

//Made by Conor on the 28/09/22 - made code to auto desplay credit text
//edited by Conor on the 29/09/2022 - change text to have thanks for playing at the start and on top, add button press to go to main menu, add coments, removed auto ending the scene

public class EndCreditsDisplay : MonoBehaviour
{
    //the text box
    [SerializeField] private TMP_Text messageText;
    //text speed
    [SerializeField] private float textSpeed;
    //the message to print
    private string[] currentMessages = { "Thanks For Playing <br><br> Programmers: <br> Tim Harrison - Lead <br> Nicholas Mueller <Br> Conor Fitton <br> <br> Game Design & Sound FX: <br> Michael Ife <br> Nick <br> <br> Artists: <br> Dee Ng - UI Design, environment and props <br> Aleksander Kony - Character <br> Chia Chee Ting <br> <br> Writer & Producer: <br> Liam Cunnane <br> <br>Music <br> Royalty Free Music by Benjamin Tissot (Bensound.com)<br> Speedier Than Photons - by Joth (Level_Music)<br> Boss Fight 2 - by Ansimuz (Boss_Music) <br><br> Special thanks <br>Those who helped playtest <br> Our friends and family  <br>     " };
    //pause menu eventSystem
    private GameObject eventSystem;
    //this scene event system
    [SerializeField] private GameObject dialogeEventSystem;
    int activeMessage = 0;

   // private bool done = false;
    
    // Start is called before the first frame update
    void Start()
    {
        //finds the eventsystem for pause menu.
        eventSystem = GameObject.Find("Menu");
        //turns off the pause menu eventsystem and this ones on
        if(eventSystem != null)
        {
            eventSystem.SetActive(false);
        }
        dialogeEventSystem.SetActive(true);
        //enables the mouse
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        //empites the text box
        messageText.text = string.Empty;
        //shows the first message
        DisplayMessage();

        

    }

    // Update is called once per frame
    void Update()
    {
        /*if(activeMessage == 2)
        {
            
            returnToMain();
        }*/
       /* if(done == false)
        {
            if (messageText.text == currentMessages[activeMessage])
            {
            
                NextMessage();
            
            
            
            }
        }*/
        
        
    }
    //displays the first message
    void DisplayMessage()
    {
        string messageToDisplay = currentMessages[activeMessage];
        
        StartCoroutine(TypeLine(messageToDisplay));
            
        


    }

    //calls the next message to be printed
    public void NextMessage()
    {
        //checks if the current message has been fully typed
        if (messageText.text == currentMessages[activeMessage])
        {
            //activeMessage++;
            //checks if the full list of messages has been typed 
            if (activeMessage < currentMessages.Length)
            {
                
                //StartCoroutine(waitSec());
                messageText.text = string.Empty;
                DisplayMessage();

            }
            else
            {
                Debug.Log("ended");
                //done = true;
                //returnToMain();
            }
        }
        


    }

    //changes the scene to mainmenu
    public void returnToMain()
    {
        
        if (messageText.text != currentMessages[activeMessage])
        {
            //fully types the message
            StopAllCoroutines();
            messageText.text = currentMessages[activeMessage];
        }
        else
        {
            //loads the mainmenu scene
            SceneManager.LoadScene(0);
        }
        
       
    }

    /*IEnumerator waitSec()
    {
        Debug.Log( "start" + Time.time);
        yield return new WaitForSecondsRealtime(5);
        
        Debug.Log("end" + Time.time);
        

    }*/

    //typewriter that displays every character one by one
    IEnumerator TypeLine(string messageToShow)
    {
        
        foreach (char c in messageToShow.ToCharArray())
        {
            
            messageText.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
        



    }
}
