using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;


//Made by Conor on the 20/08/22
//edited by conor on 29/08/22 to pause the game when dialogue is on screen
//edited by conor on the 01/09/22 to have the ability to be diabled from the menu
//edited by conor on the 11/09/22 to change how the code disables the player form diableing the playerObject to the script in the PlayerObject
//edited by conor on the 03/10/22 added a button prompt to show then dialogue is done
//Edited by Tim Harrison on 18/10/22 - Added sfx

public class DialogueManager : MonoBehaviour
{
    public Image CharacterImage;
    public TMP_Text CharacterName;
    public TMP_Text messageText;
    public RectTransform backgroundBox;
    public GameObject theBox;
    [SerializeField] private float textSpeed;
    [SerializeField] private GameObject dialogeEventSystem;
    private GameObject eventSystem;
    private GameObject playerRed, playerBlue;
    private GameObject dialogueOn;

    Message[] currentMessages;
    Character[] currentCharacter;
    int activeMessage = 0;
    public static bool isActive = false;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    [SerializeField] private GameObject aButtonSprite;


    //Message messageToDisplay;
    // Start is called before the first frame update
    void Start()
    {
        //sound manager setup
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }


        eventSystem = GameObject.Find("Menu");
        playerBlue = GameObject.Find("BluePC");
        playerRed = GameObject.Find("RedPC");
        dialogueOn = GameObject.Find("DialogueEnabler");
        
            if (dialogueOn != null)
            {
                Pause();
            }
            else
            {
                Resume();
            }
        
        
        
        
    }
    /*
    void Update()
    {
        //enables the A button when message is done
        if(messageText.text == currentMessages[activeMessage].message)
        {
            aButtonSprite.SetActive(true);
        }
        else if(messageText.text == currentMessages[activeMessage].message)
        {
            aButtonSprite.SetActive(false);
        }
    }*/


    //sets veriables for the first message
    public void OpenDialogeBox(Message[] messages, Character[] characters)
    {
        messageText.text = string.Empty;
        currentMessages = messages;
        currentCharacter = characters;
        activeMessage = 0;
        isActive = true;
        
        DisplayMessage();
        
    }

    //prints the message and player sprite 
    void DisplayMessage()
    {
        Message messageToDisplay = currentMessages[activeMessage];
        
        StartCoroutine(TypeLine(messageToDisplay.message));
        Character characterToDisplay = currentCharacter[messageToDisplay.characterID];
        CharacterName.text = characterToDisplay.name;
        CharacterImage.sprite = characterToDisplay.sprite;
    }

    //calls the next message to be printed
    public void NextMessage()
    {
        //checks if the current message has been fully typed
        if(messageText.text == currentMessages[activeMessage].message)
        {
            activeMessage++;
            //checks if the full list of messages has been typed 
            if (activeMessage < currentMessages.Length)
            {
                messageText.text = string.Empty;
                aButtonSprite.SetActive(false);
                DisplayMessage();

            }
            else
            {
                //Debug.Log("ended");
                Resume();
                
            }
        }
        else
        {
            //fully types the message
            StopAllCoroutines();
            messageText.text = currentMessages[activeMessage].message;
            aButtonSprite.SetActive(true);
        }
        
    }

    IEnumerator TypeLine(string messageToShow)
    {
        aButtonSprite.SetActive(false);
        //will type each character in the string one by one
        foreach (char c in messageToShow.ToCharArray())
        {
            PlaySound("dialogue");
            messageText.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
        aButtonSprite.SetActive(true);
    }

    /* private void PauseInput(InputAction.CallbackContext context)
     {
         if(ispaused == true)
         {
             Resume();
         }
         else
         {
             Pause();
         }


     }*/
    //enables the eventsystem that the pause menu uses when the dialogue box is deactivated 
    
    public void Resume()
    {
        dialogeEventSystem.SetActive(false);
        eventSystem.SetActive(true);
        playerRed.GetComponent<PlayerGridMovement>().enabled = true; //SetActive(true);
        playerBlue.GetComponent<PlayerGridMovement>().enabled = true; //SetActive(true);

        //Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        theBox.SetActive(false);
        isActive = false;

    }

    //diables the eventsystem that the pause menu uses when the dialogue box is active  
    private void Pause()
    {

        
        eventSystem.SetActive(false);
        dialogeEventSystem.SetActive(true);
        playerRed.GetComponent<PlayerGridMovement>().enabled = false; //SetActive(false);
        playerBlue.GetComponent<PlayerGridMovement>().enabled = false; //SetActive(false);

        //Time.timeScale = 0f;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    public bool GetIsActive()
    {
        return isActive;
    }
}
