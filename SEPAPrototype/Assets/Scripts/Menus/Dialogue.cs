using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;


//Made by Conor on the 14/08/22
//edited by Conor on the 20/08/22

public class Dialogue : MonoBehaviour
{
   
    public Message[] messages;
    public Character[] charactors;

    private bool isOpen = false;
    //private int index;

   

    public void StartDialogue()
    {
        //checks if it's the first message
        if(isOpen == false)
        {
            //calls the OpenDialogeBox function in DialogueManager
            FindObjectOfType<DialogueManager>().OpenDialogeBox(messages, charactors);
            isOpen = true;
        }
        else
        {
            //calls the NextMessage function in DialogueManager
            FindObjectOfType<DialogueManager>().NextMessage();
        }


    }

    
}
    [System.Serializable]
    public class Message
    {
        public int characterID;
        public string message;
    }
    [System.Serializable]
    public class Character
    {
        public string name;
        public Sprite sprite;
    }