using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Originally written by Nicholas Mueller - 18/9/22 - Handles closing menus when the player presses the cancel button on the keyboard or gamepad


public class MenuCancel : MonoBehaviour
{
    private InputSystemUIInputModule uiModule;
    private InputAction cancel;
    [SerializeField] private Button backButton;
    private bool gameWasPausedLastFrame = true;

    // Start is called before the first frame update
    private void Awake()
    {
        uiModule = (InputSystemUIInputModule)EventSystem.current.currentInputModule;
        cancel = uiModule.cancel.action;
    }

    private void OnDisable()
    {
        gameWasPausedLastFrame = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (cancel.WasPerformedThisFrame() && (gameWasPausedLastFrame == false || SceneManager.GetActiveScene().name == "MainMenu"))
        {
            CloseMenu();
        }

        if (PauseMenu.gameIsPaused)
        {
            gameWasPausedLastFrame = false;
        }
        else
        {
            gameWasPausedLastFrame = true;
        }
    }

    private void CloseMenu()
    {
        backButton.onClick.Invoke();
    }
}
