using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 24/5/22

public class DeathMenu : MonoBehaviour
{
    private static DeathMenu deathMenuInstance;

    [SerializeField] private GameObject deathUI;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if (deathMenuInstance == null)
        {
            deathMenuInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void TurnOnDeathUI()
    {
        deathUI.SetActive(true);
        StartCoroutine(deathScreenTimer());
    }

    private IEnumerator deathScreenTimer()
    {
        
        yield return new WaitForSeconds(0.5f);
        deathUI.SetActive(false);
    }
}
