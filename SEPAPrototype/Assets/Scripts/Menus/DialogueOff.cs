using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DialogueOff : MonoBehaviour
{

    //made by conor on the 01/09/22
    private static DialogueOff dialogueOffInstance;
    [SerializeField] private GameObject DalogueOn;
    private bool On = true;
    // Start is called before the first frame update
    private void Awake()
    {
        //Stops this object from being destroyed and ensures that there is only one
        
        
            DontDestroyOnLoad(transform.gameObject);
            if (dialogueOffInstance == null)
            {
                dialogueOffInstance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        
        
        
        //SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DialougOff()
    {
        On = !On;
        DalogueOn.SetActive(On);
    }
}
