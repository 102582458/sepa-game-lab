using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

//Originally written by Tim Harrison on 4/8/22

public class LevelSelect : MonoBehaviour
{
    [SerializeField] private int level;
    [SerializeField] private TextMeshProUGUI levelText;

    // Start is called before the first frame update
    void Start()
    {
        //Assigns level number to text box
        levelText.text = level.ToString();
    }

    //Loads scene based on level number
    public void LoadScene()
    {
        SceneManager.LoadScene("Level" + level.ToString());
    }
}
