using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//made by conor 07/08/22

public class ResolutionMenu : MonoBehaviour
{

    //list of reolutions
    List<int> widths = new List<int> {1920, 1366, 1280, 960, 568};
    List<int> heights = new List<int> {1080, 768, 720, 540, 320};
    [SerializeField] private Toggle toggle;
    [SerializeField] private TMPro.TMP_Dropdown box;
    
    private void Start()
    {
        
        //sets the fulscreen toggle box to reflect the current screen on start up
        toggle.isOn = Screen.fullScreen;

        //sets the resolution dropdown box to reflect the current screen on start up
        for (int i = 0; i < widths.Count; i++)
        {
            if(Screen.width == widths[i] && Screen.height == heights[i])
            {
                box.value = i;
            }
        }
        
    }


    //changes the resolution when called
    public void setScreenSize(int index)
    {
        bool fullscreen = Screen.fullScreen;
        int width = widths[index];
        int height = heights[index];
        Screen.SetResolution(width, height, fullscreen);

    }
    //sets the screen to full screen 
    public void SetFullscreen (bool _fullscreen)
    {
        Screen.fullScreen = _fullscreen;
    }

    
}
