using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.UI;

//Edited by Tim Harrison on 11/5/22 - Add dont destroy funcionality
//Edited by Nicholas Mueller on 12/5/22 - Fixed bug that caused the menu to re-open if in a sub-menu
//Edited by Nicholas Mueller on 14/5/22 - No longer needs an object to create the pause menu due to null references. 
//Edited by Nicholas Mueller on 22/5/22 - Updated script to use the new input system
//Edited by Nicholas Mueller on 20/10/22 - Added a function that reloads the current scene

public class PauseMenu : MonoBehaviour
{
    [System.NonSerialized] public static bool gameIsPaused = false;

    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private GameObject optionsMenuUI;
    [SerializeField] private GameObject levelSelectMenuUI;
    [SerializeField] private GameObject player1ControlsMenuUI;
    [SerializeField] private GameObject player2ControlsMenuUI;
    [SerializeField] private GameObject soundMenuUI;
    [SerializeField] private GameObject videoMenuUI;
    [SerializeField] private Button primaryButton;
    private PauseMenuControls pauseMenuControls;


    private static PauseMenu pauseMenuInstance;

    private void Awake()
    {
        //add delegate to get notification when scene is loaded
        SceneManager.sceneLoaded += OnSceneLoaded;

        //create new PauseMenuControls
        pauseMenuControls = new PauseMenuControls();

        //dont destroy menu between scenes
        DontDestroyOnLoad(transform.gameObject);

        if (pauseMenuInstance == null)
        {
            pauseMenuInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        //Calls PauseInput when the PauseButton for either keyboard or controller is pressed
        pauseMenuControls.Pause.PauseButton.performed += PauseInput;

        //initially set menu and event system to false as they are initialising in scene that already has both of these
        gameObject.SetActive(false);
        gameObject.transform.Find("EventSystem").gameObject.SetActive(false);
    }

    //Enables the player controls
    private void OnEnable()
    {
        pauseMenuControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        pauseMenuControls.Disable();
    }

    private void OnDestroy()
    {
        //cleans up listeners when destroyed 
        SceneManager.sceneLoaded -= OnSceneLoaded;
        pauseMenuControls.Pause.PauseButton.performed -= PauseInput;
    }

    //when scene loaded sets menu to active or inactive
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            gameObject.SetActive(false);
            gameObject.transform.Find("EventSystem").gameObject.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            gameObject.SetActive(true);
            gameObject.transform.Find("EventSystem").gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void PauseInput(InputAction.CallbackContext context)
    {
        if (gameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void Resume()
    {
        if (!optionsMenuUI.activeSelf && !levelSelectMenuUI.activeSelf && !player1ControlsMenuUI.activeSelf && !player2ControlsMenuUI.activeSelf && !soundMenuUI.activeSelf && !videoMenuUI.activeSelf)
        {
            pauseMenuUI.SetActive(false);
            primaryButton.Select();
            Time.timeScale = 1f;
            gameIsPaused = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void LoadMainMenu()
    {
        pauseMenuUI.SetActive(false);
        primaryButton.Select();
        Time.timeScale = 1f;
        gameIsPaused = false;
        SceneManager.LoadScene("MainMenu");
    }

    public void LevelSelect()
    {
        levelSelectMenuUI.SetActive(false);
        primaryButton.Select();
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void LoadCurrentLevel()
    {
        pauseMenuUI.SetActive(false);
        primaryButton.Select();
        Time.timeScale = 1f;
        gameIsPaused = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
