using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.InputSystem;

//Edited by Tim Harrison on 21/5/22 - has public method that can be called to reload the scene
//Edited by Nicholas Mueller on 24/08/2022 - Made it so that the players now respawn in the level instead of reloading the current level scene
//Edited by Tim Harrison on 21/9/22 - RestartLevel method now will just reload scene if it's the boss level (hardcoded as Level16)
//Edited by Tim Harrison on 27/9/22 - Implemented respawing for boss level
//Edited by Tim Harrison on 27/9/22 - added code to display lives on screen
//Edited by Nicholas Mueller on 21/9/22 - Added a player parameter to the RestartLevel method, now the method sets the player who died as inactive and then respawns them
//Edited by Nicholas Mueller on 28/09/22 - Added simple death animation
//Edited by Tim Harrison and Nicholas Mueller on 29/9/22 - Merged the two versions of this script together
//Edited by Tim Harrison on 30/9/22 - If there are no free spawn points the player will now respawn where they're standing
//Edited by Tim Harrison on 2/10/22 - Reference to boss changed to level 12
//Edited by Tim Harrison on 2/10/22 - when respawning in the boss scene the blue player increments through spawn points while the red player decrements
//Edited by Tim Harrison on 18/10/22 - Can respawn players with button press
//Edited by Nicholas Mueller on 20/10/22 - Fixed bug due to a null reference which caused the game to crash

public class ReloadSceneManager : MonoBehaviour
{
    private static ReloadSceneManager reloadSceneManagerInstance;

    private GameObject playerBlue, playerRed;
    private GameObject movePointPlayerBlue, movePointPlayerRed;
    private Vector3 playerBluePos, playerRedPos;
    private Vector3 playerBlueSize, playerRedSize;

    private GameObject[] respawnPoints;

    private int maxPlayerLives;
    private int currentPlayerLives;
    private TextMeshProUGUI LivesText;

    private float respawnTime = 0.5f;
    private float deathAnimationDuration = 0.4f;

    private string bossLevelName = "Level12";

    private PlayerControls playerControls;    

    private void Awake()
    {
        playerControls = new PlayerControls();

        //add delegate to get notification when scene is loaded
        SceneManager.sceneLoaded += SetPlayersRespawn;
        SceneManager.sceneLoaded += SetLivesText;
        SceneManager.sceneLoaded += OnSceneLoad;
        playerControls.PlayerBlue.RespawnBlue.performed += RestartLevelBlue;
        playerControls.PlayerBlue.RespawnRed.performed += RestartLevelRed;

        maxPlayerLives = 50;
        currentPlayerLives = maxPlayerLives;

        if (reloadSceneManagerInstance == null)
        {
            reloadSceneManagerInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        //Stops this object from being destroyed and ensures that there is only one
        DontDestroyOnLoad(transform.gameObject);
    }

    //Enables the player controls
    private void OnEnable()
    {
        playerControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        playerControls.Disable();
    }

    private void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        currentPlayerLives = maxPlayerLives;
    }

    private void OnDestroy()
    {
        //cleans up listeners when destroyed 
        SceneManager.sceneLoaded -= SetPlayersRespawn;
        SceneManager.sceneLoaded -= SetLivesText;
        SceneManager.sceneLoaded -= OnSceneLoad;
        playerControls.PlayerBlue.RespawnBlue.performed -= RestartLevelBlue;
        playerControls.PlayerBlue.RespawnRed.performed -= RestartLevelRed;
    }

    //Sets text in lives text tmp
    private void SetLivesText(Scene scene, LoadSceneMode mode)
    {
        //Gets Lives text in boss scene
        if (SceneManager.GetActiveScene().name == bossLevelName)
        {
            LivesText = GameObject.Find("LivesText").GetComponent<TextMeshProUGUI>();
            LivesText.text = $"Lives: {currentPlayerLives}";
            LivesText.color = Color.green;
        }
    }

    //Sets the player to the starting position of the level
    public void RestartLevel(GameObject playerGameObject)
    {
        //if the scene is Level16 "The boss level" the scene is simply reloaded
        if (SceneManager.GetActiveScene().name == bossLevelName)
        {
            if (respawnPoints.Length != 0)
            {
                if (currentPlayerLives > 1)
                {
                    if (playerGameObject == playerBlue)
                    {
                        playerBlue.GetComponent<CommandManager>().ForceDropItem();
                        playerBlue.GetComponent<PlayerGridMovement>().enabled = false;
                        StartCoroutine(BossRespawnPlayer(playerBlue));
                        StartCoroutine(respawnAnimationCoroutine(playerGameObject));
                        currentPlayerLives--;
                        if (LivesText != null) { LivesText.text = $"Lives: {currentPlayerLives}"; }
                    }
                    else if (playerGameObject == playerRed)
                    {
                        playerRed.GetComponent<CommandManager>().ForceDropItem();
                        playerRed.GetComponent<CommandManager>().ForceDropItem();
                        playerRed.GetComponent<PlayerGridMovement>().enabled = false;
                        StartCoroutine(BossRespawnPlayer(playerRed));
                        StartCoroutine(respawnAnimationCoroutine(playerGameObject));
                        currentPlayerLives--;
                        if (LivesText != null) { LivesText.text = $"Lives: {currentPlayerLives}"; }
                    }

                    if (currentPlayerLives <= 10) { LivesText.color = Color.red; }
                }
                else
                {
                    currentPlayerLives = maxPlayerLives;
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }                
            }
            else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else
        {
            if (playerGameObject == playerBlue)
            {
                playerBlue.GetComponent<CommandManager>().ForceDropItem();
                playerBlue.GetComponent<PlayerGridMovement>().enabled = false;
            }
            else if (playerGameObject == playerRed)
            {
                playerRed.GetComponent<CommandManager>().ForceDropItem();
                playerRed.GetComponent<PlayerGridMovement>().enabled = false;
            }
            StartCoroutine(respawnPlayerCoroutine(playerGameObject));
            StartCoroutine(respawnAnimationCoroutine(playerGameObject));
        }
    }

    //Called when button is pressed to respawn blue player
    public void RestartLevelBlue(InputAction.CallbackContext context)
    {
        GameObject playerGameObject = playerBlue;
        RestartLevel(playerGameObject);
    }

    //Called when button is pressed to respawn red player
    public void RestartLevelRed(InputAction.CallbackContext context)
    {
        GameObject playerGameObject = playerRed;
        RestartLevel(playerGameObject);
    }

    //Iterates through possible spawn points and moves player there when it finds a free one
    private IEnumerator BossRespawnPlayer(GameObject player)
    {
        bool canRespawn = false;

        yield return new WaitForSeconds(respawnTime);

        if (player == playerBlue)
        {
            for (int i = 0; i < respawnPoints.Length; i++)
            {
                RaycastHit2D spawnCheck = Physics2D.BoxCast(respawnPoints[i].transform.position, new Vector2(0.95f, 0.95f), 0, new Vector2(0, 0), 0);
                if (spawnCheck.collider == null)
                {
                    canRespawn = true;
                    player.gameObject.transform.position = respawnPoints[i].transform.position;
                    movePointPlayerBlue.transform.position = respawnPoints[i].transform.position;
                    player.gameObject.transform.localScale = playerBlueSize;
                    player.GetComponent<PlayerGridMovement>().ChangePlayerRotation("Right");
                    player.GetComponent<PlayerGridMovement>().enabled = true;
                }
            }
            //If all spawn points are blocked player respawns where they are
            if (!canRespawn)
            {
                player.gameObject.transform.localScale = playerBlueSize;
                player.GetComponent<PlayerGridMovement>().enabled = true;
            }
        }

        if (player == playerRed)
        {
            for (int i = respawnPoints.Length - 1; i > -1; i--)
            {
                RaycastHit2D spawnCheck = Physics2D.BoxCast(respawnPoints[i].transform.position, new Vector2(0.95f, 0.95f), 0, new Vector2(0, 0), 0);
                if (spawnCheck.collider == null)
                {
                    canRespawn = true;
                    player.gameObject.transform.position = respawnPoints[i].transform.position;
                    movePointPlayerRed.transform.position = respawnPoints[i].transform.position;
                    player.gameObject.transform.localScale = playerBlueSize;
                    player.GetComponent<PlayerGridMovement>().ChangePlayerRotation("Right");
                    player.GetComponent<PlayerGridMovement>().enabled = true;
                }
            }
            //If all spawn points are blocked player respawns where they are
            if (!canRespawn)
            {
                player.gameObject.transform.localScale = playerBlueSize;
                player.GetComponent<PlayerGridMovement>().enabled = true;
            }
        }
    }

    //Using an IEnumerator to enable player controls after the death screen UI
    private IEnumerator respawnPlayerCoroutine(GameObject playerGameObject)
    {
        
        yield return new WaitForSeconds(respawnTime);

        if (playerGameObject == playerBlue)
        {
            if (CheckFirstSpawnLocation(playerBlue))
            {
                movePointPlayerBlue.transform.position = playerBluePos;
                playerBlue.gameObject.transform.position = playerBluePos;
                playerBlue.gameObject.transform.localScale = playerBlueSize;
            } else
            {
                movePointPlayerBlue.transform.position = playerRedPos;
                playerBlue.gameObject.transform.position = playerRedPos;
                playerBlue.gameObject.transform.localScale = playerBlueSize;
            }
            playerBlue.GetComponent<PlayerGridMovement>().ChangePlayerRotation("Right");
            playerBlue.GetComponent<PlayerGridMovement>().enabled = true;
        }
        else if (playerGameObject == playerRed)
        {
            if (CheckFirstSpawnLocation(playerRed))
            {
                movePointPlayerRed.transform.position = playerRedPos;
                playerRed.gameObject.transform.position = playerRedPos;
                playerRed.gameObject.transform.localScale = playerRedSize;
            }
            else
            {
                movePointPlayerRed.transform.position = playerBluePos;
                playerRed.gameObject.transform.position = playerBluePos;
                playerRed.gameObject.transform.localScale = playerRedSize;
            }
            playerRed.GetComponent<PlayerGridMovement>().ChangePlayerRotation("Right");
            playerRed.GetComponent<PlayerGridMovement>().enabled = true;
        }
    }

    //Sets player spawn variables
    private void SetPlayersRespawn(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().name != "MainMenu" && SceneManager.GetActiveScene().name != bossLevelName)
        {
            GetPlayerAndMovePointValues();

            playerBluePos = playerBlue.gameObject.transform.position;
            playerBlueSize = playerBlue.gameObject.transform.localScale;
            playerRedPos = playerRed.gameObject.transform.position;
            playerRedSize = playerRed.gameObject.transform.localScale;
        }
        if (SceneManager.GetActiveScene().name == bossLevelName)
        {
            GetPlayerAndMovePointValues();

            playerBluePos = playerBlue.gameObject.transform.position;
            playerBlueSize = playerBlue.gameObject.transform.localScale;
            playerRedPos = playerRed.gameObject.transform.position;
            playerRedSize = playerRed.gameObject.transform.localScale;
        }
    }

    private bool CheckFirstSpawnLocation(GameObject player)
    {
        if (player.gameObject.name == "BluePC")
        {
            if (!Physics2D.OverlapBox(playerBluePos, new Vector2(0.95f, 0.95f), 0f, player.GetComponent<PlayerGridMovement>().layerMaskThatStopsMovement))
            {
                return true;
            }
        } else if (player.gameObject.name == "RedPC")
        {
            if (!Physics2D.OverlapBox(playerRedPos, new Vector2(0.95f, 0.95f), 0f, player.GetComponent<PlayerGridMovement>().layerMaskThatStopsMovement))
            {
                return true;
            }
        }

        return false;
    }

    //Set initial values for boss level
    public void SetPlayersRespawnBossFight()
    {
        respawnPoints = GameObject.FindGameObjectsWithTag("RespawnPoint");

        GetPlayerAndMovePointValues();
    }

    //Set inital values
    private void GetPlayerAndMovePointValues()
    {
        playerBlue = GameObject.Find("BluePC");
        playerRed = GameObject.Find("RedPC");
        movePointPlayerBlue = GameObject.Find("MovePointBlue");
        movePointPlayerRed = GameObject.Find("MovePointRed");
    }

    private IEnumerator respawnAnimationCoroutine(GameObject playerGameObject)
    {
        float timer = 0.0f;
        float duration = deathAnimationDuration;
        Vector3 initialSize = playerGameObject.transform.localScale;
        Vector3 initialRotation = playerGameObject.transform.eulerAngles;

        while (timer < duration)
        {
            timer += Time.deltaTime;
            float t = timer / duration;
            //smoother step algorithm
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            playerGameObject.transform.localScale = Vector3.Lerp(initialSize, new Vector3(0f, 0f, 0f), t);
            playerGameObject.transform.eulerAngles = Vector3.Lerp(initialRotation, initialRotation + new Vector3(0f, 0f, 360f), t);
            yield return null;
        }

        yield return null;
    }
}
