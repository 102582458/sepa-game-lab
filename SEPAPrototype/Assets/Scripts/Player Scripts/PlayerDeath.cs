using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on 21/5/22
//Edited by Tim Harrison on 24/5/22 - add ontrigger method and collated all collisions and triggers to call one function that reloads the scene
//Edited by Tim Harrison on 25/9/22 - Kill player functionality moved into public function so other objects can kill player
//Edited by Tim Harrison on 27/9/22 - Added overload for KillPlayer() method that is called by the laser beam
//Edited by Nicholas Mueller 28/09/22 - Added a respawn timer check
//Edited by Tim Harrison on 18/10/22 - added sfx on death

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] private List<string> DeadlyObjects;

    private GameObject reloadSceneManagerObj;
    private ReloadSceneManager reloadSceneManager;

    private bool playerRespawning = false;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    // Start is called before the first frame update
    void Start()
    {
        //sound manager setup
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }

        //gets reload scenemanager
        reloadSceneManagerObj = GameObject.FindGameObjectWithTag("ReloadSceneManager");
        if (reloadSceneManagerObj != null)
        {
            reloadSceneManager = reloadSceneManagerObj.GetComponent<ReloadSceneManager>();
        }
    }

    //checks if there is collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        CheckDeath(collision.gameObject);
    }

    //checks if there is trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        CheckDeath(collision.gameObject);
    }


    //checks if collsion/trigger is deadly object and reloads the scene if so
    private void CheckDeath(GameObject collisionObj)
    {
        if (reloadSceneManager != null)
        {
            foreach (string s in DeadlyObjects)
            {
                if (collisionObj.CompareTag(s))
                {
                    KillPlayer();
                }
            }
        }
    }

    public void KillPlayer()
    {
        if (!playerRespawning)
        {
            PlaySound("playerDeath");
            reloadSceneManager.RestartLevel(this.gameObject);
            StartCoroutine(respawnTimer());
        }        
    }

    //Overload for laser beam
    //public void KillPlayer(GameObject playerObj)
    //{
    //    reloadSceneManager.RestartLevel(playerObj);
    //    StartCoroutine(respawnTimer());
    //}

    private IEnumerator respawnTimer()
    {
        playerRespawning = true;
        yield return new WaitForSeconds(0.6f);
        playerRespawning = false;
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }
}
