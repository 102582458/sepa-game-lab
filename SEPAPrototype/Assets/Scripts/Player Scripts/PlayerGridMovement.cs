using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

//Edited by Nicholas Mueller on 7/5/22 - Commented out playerIsMoving variable as it is currently not in use & made newTileMovePoint variable public
//Edited by Nicholas Mueller on 20/5/22 - Added a speed multiplier variable depending on the tile data that the player is standing on
//Edited by Nicholas Mueller on 22/5/22 - Updated script to use the new input system, now gets player input from the PlayerInputController script
//Edited by Tim Harrison on 20/8/22 - Added functionality to play sound and calls to play ice sounds
//Edited by Nicholas Mueller on 23/08/2022 - Added the ability to change direction without moving using either the Shift Key or the Left Trigger
//Edited by Nicholas Mueller on 28/09/2022 - Seperated laymasks that stop movement while holding objects and when not
//Edited by Tim Harrison on 24/10/22 - Implemented movement animations

public class PlayerGridMovement : MonoBehaviour
{
    //Stores player movement input
    private PlayerControls playerControls;
    //Stores unique player movement for either player
    private InputAction movement, changeDirectionInput;
    //Stores player faction
    [SerializeField] private string faction;
    //Stores player movement speed
    [SerializeField] private float moveSpeed = 3f;
    //Stores a move point which is used to moved the player 
    [SerializeField] private Transform newTileMovePoint;
    //Is a layermask of different layers that should stop player movement
    public LayerMask layerMaskThatStopsMovement;
    //Is a layermask of different layers that should stop player movement while holding objects
    public LayerMask layerMaskThatStopsMovementWithObjects;

    //Public variable for a validation check in the BoxMove script which determines if the player is currently moving
    //[System.NonSerialized] public bool playerIsMoving = false;

    //Variable that records if the player is holding an object or not
    private bool holdingObject = false;
    //Variable to record the last tile that the player moved on
    private Vector3 oldTileMovePoint;

    //Variables assoicated with tilemap data 
    [SerializeField] private TilemapDataManager tilemapDataManager;
    private bool playerOnIce = false;
    private float speedMultiplier = 1f;
    //Bug fix variable for ice movement
    private bool collisionOnIce = false;

    //stores the direction that the player is currently facing
    private int currentAngle;

    //A 2D array that gives the angle/90 that the player needs to turn to reach the desired direction.
    private int[,] directionAngles = new int[4, 4] { { 0, 1, 2, -1 }, { -1, 0, 1, 2 }, { 2, -1, 0, 1 }, { 1, 2, -1, 0 } };

    //Dictionary that stores a string indicating direction and an integer that refers to the tuple in the 2D array that determines turning angles from that direction.
    private Dictionary<string, int> direction = new Dictionary<string, int>();

    private int allGamepads, currentGamepads;

    private GameObject soundManagerObj;
    private SoundManager soundManager;
    private bool playIceSFX = true;

    //player sprite renderer
    private SpriteRenderer spriteRenderer;
    //player game object that holds the sprite
    [SerializeField] private GameObject playerSprite;
    //player sprites 
    [SerializeField] private Sprite leftSprite, rightSprite, downSprite, upSprite;

    private Animator animator;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        animator.SetFloat("IdleRotation", 1);

        newTileMovePoint.parent = null;
        //Assigns variable with default player angle
        currentAngle = 0;
        //Adds respective key and value to the direction dictionary 
        direction.Add("right", 0);
        direction.Add("up", 1);
        direction.Add("left", 2);
        direction.Add("down", 3);
        //Sets the player controls for the start of the game
        SetControls();
    }

    private void Start()
    {
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }
        //sets the sprite renderer
        spriteRenderer = playerSprite.GetComponent<SpriteRenderer>();
    }

    //Enables the player controls
    private void OnEnable()
    {
        playerControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        playerControls.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        //Gets called to check if change in player input 
        CheckInput();
        //Updates if tilemap data is being used 
        if (tilemapDataManager)
        {
            playerOnIce = tilemapDataManager.IsIce(transform.position);
            speedMultiplier = tilemapDataManager.GetSpeedMultiplier(transform.position);

            //If player enters ice play an ice sound (playIceSFX is also set in changePlayerRotation so will play a sound whenever player moves on ice from input too)
            if (playerOnIce && playIceSFX)
            {
                int rng = Random.Range(0, 2);
                switch (rng)
                {
                    case 0:
                        PlaySound("iceSlip1");
                        break;
                    case 1:
                        PlaySound("iceSlip2");
                        break;
                }

                playIceSFX = false;
            }
            else if (!playerOnIce)
            {
                playIceSFX = true;
            }
        }

        //Checks a variable if the player is holding a box from the BoxMove script attached to the player
        holdingObject = gameObject.GetComponent<CommandManager>().GetHoldingObject();

        //Causes the player to move towards the move point
        transform.position = Vector2.MoveTowards(transform.position, newTileMovePoint.position, moveSpeed * speedMultiplier * Time.deltaTime);

        //Determines if the player is moving or not
        /*if (transform.position == newTileMovePoint.position)
        {
            playerIsMoving = false;
        } else
        {
            playerIsMoving = true;
        }*/

        //Handles player movement when on ice
        if (playerOnIce && Vector2.Distance(transform.position, newTileMovePoint.position) <= .05f && !collisionOnIce)
        {
            SetIdleAnimation();
            switch (currentAngle)
            {
                case 0: //Handles movement in the right direction
                    if (holdingObject == true)
                    {
                        MovePlayerOnIce(new Vector3(1f * 1.5f, 0f, 0f), new Vector2(1.95f, 0.95f), new Vector3(1f, 0f, 0f), layerMaskThatStopsMovementWithObjects);
                    }
                    else
                    {
                        MovePlayerOnIce(new Vector3(1f, 0f, 0f), new Vector2(0.95f, 0.95f), new Vector3(1f, 0f, 0f), layerMaskThatStopsMovement);
                    }
                    break;
                case 1: //Handles movement in the up direction
                    if (holdingObject == true)
                    {
                        MovePlayerOnIce(new Vector3(0f, 1f * 1.5f, 0f), new Vector2(0.95f, 1.95f), new Vector3(0f, 1f, 0f), layerMaskThatStopsMovementWithObjects);
                    }
                    else
                    {
                        MovePlayerOnIce(new Vector3(0f, 1f, 0f), new Vector2(0.95f, 0.95f), new Vector3(0f, 1f, 0f), layerMaskThatStopsMovement);
                    }
                    break;
                case 2: //Handles movement in the left direction
                    if (holdingObject == true)
                    {
                        MovePlayerOnIce(new Vector3(-1f * 1.5f, 0f, 0f), new Vector2(1.95f, 0.95f), new Vector3(-1f, 0f, 0f), layerMaskThatStopsMovementWithObjects);
                    }
                    else
                    {
                        MovePlayerOnIce(new Vector3(-1f, 0f, 0f), new Vector2(0.95f, 0.95f), new Vector3(-1f, 0f, 0f), layerMaskThatStopsMovement);
                    }
                    break;
                case 3: //Handles movement in the down direction
                    if (holdingObject == true)
                    {
                        MovePlayerOnIce(new Vector3(0f, -1f * 1.5f, 0f), new Vector2(0.95f, 1.95f), new Vector3(0f, -1f, 0f), layerMaskThatStopsMovementWithObjects);
                    } else
                    {
                        MovePlayerOnIce(new Vector3(0f, -1f, 0f), new Vector2(0.95f, 0.95f), new Vector3(0f, -1f, 0f), layerMaskThatStopsMovement);
                    }
                    break;
            }
        }
        //Handles regular player movement & player direction on the spot
        if (Vector2.Distance(transform.position, newTileMovePoint.position) <= .05f)
        {
            if (movement.ReadValue<Vector2>() == new Vector2(0f, 0f))
            {
                SetIdleAnimation();
            } else if (Mathf.Abs(movement.ReadValue<Vector2>()[0]) > Mathf.Abs(movement.ReadValue<Vector2>()[1]))
            {
                if (movement.ReadValue<Vector2>()[0] > 0f)
                {
                    if (changeDirectionInput.ReadValue<float>() >= 0.8)
                    {
                        ChangePlayerRotation("Right");
                    } else
                    {
                        ChangePlayerRotation("Right");
                        if (holdingObject == true)
                        {
                            MovePlayerHoldingObject("Right");
                        }
                        else
                        {
                            MovePlayer("Right");
                        }                        
                    }
                } else
                {
                    if (changeDirectionInput.ReadValue<float>() >= 0.8)
                    {
                        ChangePlayerRotation("Left");
                    }
                    else
                    {
                        ChangePlayerRotation("Left");
                        if (holdingObject == true)
                        {
                            MovePlayerHoldingObject("Left");
                        }
                        else
                        {
                            MovePlayer("Left");
                        }                        
                    }
                }
            } else if (Mathf.Abs(movement.ReadValue<Vector2>()[1]) > Mathf.Abs(movement.ReadValue<Vector2>()[0]))
            {
                if (movement.ReadValue<Vector2>()[1] > 0f)
                {
                    if (changeDirectionInput.ReadValue<float>() >= 0.8)
                    {
                        ChangePlayerRotation("Up");
                    }
                    else
                    {
                        ChangePlayerRotation("Up");
                        if (holdingObject == true)
                        {
                            MovePlayerHoldingObject("Up");
                        }
                        else
                        {
                            MovePlayer("Up");
                        }                        
                    }
                } else
                {
                    if (changeDirectionInput.ReadValue<float>() >= 0.8)
                    {
                        ChangePlayerRotation("Down");
                    }
                    else
                    {
                        ChangePlayerRotation("Down");
                        if (holdingObject == true)
                        {
                            MovePlayerHoldingObject("Down");
                        }
                        else
                        {
                            MovePlayer("Down");
                        }                        
                    }
                }
            }
        }

        //stops the player sprite from rotating
        playerSprite.transform.rotation = Quaternion.Euler(0f, 0f, transform.rotation.z * -1);
        
        
        //Old Movement with previous Input System
        /*if (Vector2.Distance(transform.position, newTileMovePoint.position) <= .05f)
        {
            if (Mathf.Abs(Input.GetAxisRaw(characterAxisX)) == 1f)
            {
                //code that checks which way the character is turning on the X-Axes that calls the ChangePlayerRotation
                if (Input.GetAxisRaw(characterAxisX) == 1)
                {
                    ChangePlayerRotation("Right");
                }
                else if (Input.GetAxisRaw(characterAxisX) == -1)
                {
                    ChangePlayerRotation("Left");
                }

                if (holdingObject == true)
                {
                    MovePlayerHoldingObject(true);
                }
                else
                {
                    MovePlayer(true);
                }
            }
            else if (Mathf.Abs(Input.GetAxisRaw(characterAxisY)) == 1f)
            {
                //code that checks which way the character is turning on the Y-Axes that calls the ChangePlayerRotation
                if (Input.GetAxisRaw(characterAxisY) == 1)
                {
                    ChangePlayerRotation("Up");
                }
                else if (Input.GetAxisRaw(characterAxisY) == -1)
                {
                    ChangePlayerRotation("Down");
                }

                if (holdingObject == true)
                {
                    MovePlayerHoldingObject(false);
                }
                else
                {
                    MovePlayer(false);
                }
            }
        }*/


    }

    //Gets called to update player controls 
    private void SetControls()
    {
        if (faction == "blue")
        {
             playerControls = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>().GetBlueCharacterControls();
             movement = playerControls.PlayerBlue.Movement;
             changeDirectionInput = playerControls.PlayerBlue.Shift;
        }
        else if (faction == "red")
        {
            playerControls = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>().GetRedCharacterControls();
            movement = playerControls.PlayerRed.Movement;
            changeDirectionInput = playerControls.PlayerRed.Shift;
        }
    }
    
    //Checks if the number of gamepads have changed
    private void CheckInput()
    {
        allGamepads = Gamepad.all.Count;
        if (allGamepads != currentGamepads)
        {
            currentGamepads = allGamepads;
            SetControls();
        }
    }

    public void MovePlayerOnIce(Vector3 originPoint, Vector2 boxCastSize, Vector3 newMovePoint, LayerMask layerMaskThatStopsMovement)
    {
        //Handles player ice movement in a specified direction & while holding objects 
        if (!Physics2D.OverlapBox(newTileMovePoint.position + originPoint, boxCastSize, 0f, layerMaskThatStopsMovement))
        {
            oldTileMovePoint = newTileMovePoint.position;
            newTileMovePoint.position += newMovePoint;
        }
        else
        {
            collisionOnIce = true;
        }
    }
    public void MovePlayer(string moveDirection)
    {
        if (moveDirection == "Right")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(1f, 0f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovement))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(1f, 0f, 0f);
                collisionOnIce = false;
                ChangeSprite(rightSprite);

                SetAnimation("Horizontal", 0, 1, 1, 1);
            }
        } else if (moveDirection == "Left")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(-1f, 0f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovement))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(-1f, 0f, 0f);
                collisionOnIce = false;
                ChangeSprite(leftSprite);

                SetAnimation("Horizontal", 0, 1, -1, 0);
            }
        }
        else if (moveDirection == "Up")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, 1f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovement))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(0f, 1f, 0f);
                collisionOnIce = false;
                ChangeSprite(upSprite);

                SetAnimation("Vertical", 1, 0, 1, 3);
            }
        }
        else if (moveDirection == "Down")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, -1f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovement))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(0f, -1f, 0f);
                collisionOnIce = false;
                ChangeSprite(downSprite);

                SetAnimation("Vertical", 1, 0, -1, 2);
            }
        }
    }

    public void MovePlayerHoldingObject(string moveDirection)
    {
        if (moveDirection == "Right")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(1f * 1.5f, 0f, 0f), new Vector2(1.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(1f, 0f, 0f);
                collisionOnIce = false;
                ChangeSprite(rightSprite);

                SetAnimation("Horizontal", 0, 1, 1, 1);
            }
        }
        else if (moveDirection == "Left")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(-1f * 1.5f, 0f, 0f), new Vector2(1.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(-1f, 0f, 0f);
                collisionOnIce = false;
                ChangeSprite(leftSprite);

                SetAnimation("Horizontal", 0, 1, -1, 0);
            }
        }
        else if (moveDirection == "Up")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, 1f * 1.5f, 0f), new Vector2(0.95f, 1.95f), 0f, layerMaskThatStopsMovementWithObjects))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(0f, 1f, 0f);
                collisionOnIce = false;
                ChangeSprite(upSprite);

                SetAnimation("Vertical", 1, 0, 1, 3);
            }
        }
        else if (moveDirection == "Down")
        {
            if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, -1f * 1.5f, 0f), new Vector2(0.95f, 1.95f), 0f, layerMaskThatStopsMovementWithObjects))
            {
                oldTileMovePoint = newTileMovePoint.position;
                newTileMovePoint.position += new Vector3(0f, -1f, 0f);
                collisionOnIce = false;
                ChangeSprite(downSprite);

                SetAnimation("Vertical", 1, 0, -1, 2);
            }
        }
    }

    //Sets values for animator to play the correct animation based on movement
    private void SetAnimation(string direction, float VSpeed, float HSpeed, float directionValue, float idleRotation)
    {
        animator.SetFloat("VSpeed", VSpeed);
        animator.SetFloat("HSpeed", HSpeed);
        animator.SetFloat(direction, directionValue);
        animator.SetFloat("IdleRotation", idleRotation);
    }

    //Set values to play idle animation
    private void SetIdleAnimation()
    {
        animator.SetFloat("HSpeed", 0);
        animator.SetFloat("VSpeed", 0);
    }

    public void ChangePlayerRotation(string playerDirection)
    {
        //Handles changing the player model direction if they are not holding an object
        if (!holdingObject)
        {
            switch (playerDirection)
            {
                case "Up":
                    currentAngle = FlipPlayer(directionAngles[currentAngle, 1], currentAngle, direction["up"]);
                    ChangeSprite(upSprite);
                    animator.SetFloat("IdleRotation", 3);
                    break;
                case "Down":
                    currentAngle = FlipPlayer(directionAngles[currentAngle, 3], currentAngle, direction["down"]);
                    ChangeSprite(downSprite);
                    animator.SetFloat("IdleRotation", 2);
                    break;
                case "Right":
                    currentAngle = FlipPlayer(directionAngles[currentAngle, 0], currentAngle, direction["right"]);
                    ChangeSprite(rightSprite);
                    animator.SetFloat("IdleRotation", 1);
                    break;
                case "Left":
                    currentAngle = FlipPlayer(directionAngles[currentAngle, 2], currentAngle, direction["left"]);
                    ChangeSprite(leftSprite);
                    animator.SetFloat("IdleRotation", 0);
                    break;
            }
        } else //Handles changing the player model direction when they are holding an object
        {
            switch (playerDirection)
            {
                case "Up":
                    if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, 1f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
                    {
                        currentAngle = FlipPlayer(directionAngles[currentAngle, 1], currentAngle, direction["up"]);
                        ChangeSprite(upSprite);
                        animator.SetFloat("IdleRotation", 3);
                    }
                    break;
                case "Down":
                    if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(0f, -1f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
                    {
                        currentAngle = FlipPlayer(directionAngles[currentAngle, 3], currentAngle, direction["down"]);
                        ChangeSprite(downSprite);
                        animator.SetFloat("IdleRotation", 2);
                    }
                    break;
                case "Right":
                    if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(1f, 0f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
                    {
                        currentAngle = FlipPlayer(directionAngles[currentAngle, 0], currentAngle, direction["right"]);
                        ChangeSprite(rightSprite);
                        animator.SetFloat("IdleRotation", 1);
                    }
                    break;
                case "Left":
                    if (!Physics2D.OverlapBox(newTileMovePoint.position + new Vector3(-1f, 0f, 0f), new Vector2(0.95f, 0.95f), 0f, layerMaskThatStopsMovementWithObjects))
                    {
                        currentAngle = FlipPlayer(directionAngles[currentAngle, 2], currentAngle, direction["left"]);
                        ChangeSprite(leftSprite);
                        animator.SetFloat("IdleRotation", 0);
                    }
                    break;
            }
        }

        //sets bool to true so ice sound will be played after player moves on ice from input
        playIceSFX = true;
    }

    public int FlipPlayer(int angle, int currentAngle, int newDirection)
    {
        //playerFacingRight = !playerFacingRight;
        transform.Rotate(0, 0, angle * 90);
        return currentAngle = newDirection;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*Complex way of checking if a layer is in a layermask 
        Checks to see if the player collides with an object with a layer that it shouldn't be able to collide with
        if so, sends the player back to its old position*/
        if (((1<<collision.gameObject.layer) & layerMaskThatStopsMovement) != 0)
        {
            newTileMovePoint.position = oldTileMovePoint;
            if (playerOnIce)
            {
                collisionOnIce = true;
            }
        }
    }

    //Getters and setters for various variables
    public Transform GetNewTileMovePoint()
    {
        return newTileMovePoint;
    }

    //tells sound manager to play sound
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    private void ChangeSprite(Sprite newSprite)
    {
        spriteRenderer.sprite = newSprite;
    }
}
