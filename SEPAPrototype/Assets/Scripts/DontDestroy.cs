using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    private static DontDestroy dontDestroyInstance;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if (dontDestroyInstance == null)
        { 
            dontDestroyInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
