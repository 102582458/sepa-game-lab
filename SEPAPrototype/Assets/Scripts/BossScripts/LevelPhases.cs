using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//Written by Tim Harrison on - 29/8/22
//Edited by Tim Harrison on 12/9/22 - Added struct to hold data and implemented timer for attack phases
//Edited by Tim Harrison on 23/9/22 - Now destroys objects that migth have been moved by the player out of their phase list and fire on phase start
//Edited by Tim Harrison on 25/9/22 - Now destroys all lasers at the end of each phase
//Edited by Tim Harrison on 25/9/22 - Gets refernece to players and command manager and makes them drop items on end of phase
//Edited by Tim Harrison on 27/9/22 - Has reference to reload scene manager to tell it when to get new spawn points
//Edited by Tim Harrison on 28/9/22 - Has reference to tiledatamanger and sets the map to be used
//Edited by Tim Harrison on 17/10/22 - Button can now be pressed to skip through phases
//Edited by Tim Harrison on 25/10/22 - Added reference to boss object and sets variable on final phase

public class LevelPhases : MonoBehaviour
{
    [SerializeField] private GameObject RedPC;
    private CommandManager redCM;
    [SerializeField] private GameObject BluePC;
    private CommandManager blueCM;

    private GameObject reloadSceneManagerObj;
    private ReloadSceneManager reloadSceneManager;
    private PlayerControls playerControls;

    [SerializeField] private TilemapDataManager tilemapDataManager;

    [SerializeField] GameObject boss;


    //Holds data for each phase
    private struct PhaseData
    {
        public PhaseType phaseType;
        public List<GameObject> objectList;        
        public float attackPhaseLength;

        public PhaseData(PhaseType phaseType, List<GameObject> objectList, float attackPhaseLength)
        {
            this.objectList = objectList;
            this.phaseType = phaseType;
            this.attackPhaseLength = attackPhaseLength;
        }
    }

    private enum PhaseType
    {
        Puzzle,
        Attack,
    }

    [Header("Phase One")]
    [SerializeField] private PhaseType Type1;
    [SerializeField] private List<GameObject> List1 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength1;

    [Header("Phase Two")]
    [SerializeField] private PhaseType Type2;
    [SerializeField] private List<GameObject> List2 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength2;

    [Header("Phase Three")]
    [SerializeField] private PhaseType Type3;
    [SerializeField] private List<GameObject> List3 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength3;

    [Header("Phase Four")]
    [SerializeField] private PhaseType Type4;
    [SerializeField] private List<GameObject> List4 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength4;

    [Header("Phase Five")]
    [SerializeField] private PhaseType Type5;
    [SerializeField] private List<GameObject> List5 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength5;

    [Header("Phase Six")]
    [SerializeField] private PhaseType Type6;
    [SerializeField] private List<GameObject> List6 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength6;

    [Header("Phase Seven")]
    [SerializeField] private PhaseType Type7;
    [SerializeField] private List<GameObject> List7 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength7;

    [Header("Phase Eight")]
    [SerializeField] private PhaseType Type8;
    [SerializeField] private List<GameObject> List8 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength8;

    [Header("Phase Nine")]
    [SerializeField] private PhaseType Type9;
    [SerializeField] private List<GameObject> List9 = new List<GameObject>();
    [SerializeField] private float attackPhaseLength9;

    private List<string> objectsToDestroy = new List<string>();
    private List<PhaseData> listOfPhaseData = new List<PhaseData>();

    private int currentPhase = 1;

    //Enables the player controls
    private void OnEnable()
    {
        playerControls.Enable();
    }

    //Disables the player controls
    private void OnDisable()
    {
        playerControls.Disable();
    }

    void Awake()
    {
        SetControls();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Gets reload scene manager reference
        reloadSceneManagerObj = GameObject.FindGameObjectWithTag("ReloadSceneManager");
        if (reloadSceneManagerObj != null)
        {
            reloadSceneManager = reloadSceneManagerObj.GetComponent<ReloadSceneManager>();
        }

        //Gets spawn points for the first wave
        reloadSceneManager.SetPlayersRespawnBossFight();

        redCM = RedPC.GetComponent<CommandManager>();
        blueCM = BluePC.GetComponent<CommandManager>();

        //Creates all structs and adds them to a list
        listOfPhaseData.Add(new PhaseData(Type1, List1, attackPhaseLength1));
        listOfPhaseData.Add(new PhaseData(Type2, List2, attackPhaseLength2));
        listOfPhaseData.Add(new PhaseData(Type3, List3, attackPhaseLength3));
        listOfPhaseData.Add(new PhaseData(Type4, List4, attackPhaseLength4));
        listOfPhaseData.Add(new PhaseData(Type5, List5, attackPhaseLength5));
        listOfPhaseData.Add(new PhaseData(Type6, List6, attackPhaseLength6));
        listOfPhaseData.Add(new PhaseData(Type7, List7, attackPhaseLength7));
        listOfPhaseData.Add(new PhaseData(Type8, List8, attackPhaseLength8));
        listOfPhaseData.Add(new PhaseData(Type9, List9, attackPhaseLength9));

        objectsToDestroy.Add("BlueBox");
        objectsToDestroy.Add("RedBox");
        objectsToDestroy.Add("NeutralBox");
        objectsToDestroy.Add("Fire");
        objectsToDestroy.Add("FireExtinquisher");
        objectsToDestroy.Add("Projectile");

        //Sets up phase 1
        for (int i = 0; i < listOfPhaseData.Count; i++)            
        {
            if (i != 0)
            {
                foreach(GameObject g in listOfPhaseData[i].objectList)
                {
                    g.SetActive(false);
                }
            }
        }
        //tells tile data manager which map to use
        tilemapDataManager.SetActiveMap(1);
    }

    private void Update()
    {
        //Destroys all lasers at the end of each phase
        GameObject[] lasers = GameObject.FindGameObjectsWithTag("Laser");
        if (lasers.Length > 0)
        {
            foreach (GameObject g in lasers)
            {
                Destroy(g);
            }
        }
    }

    //set all objects in the list of the new phase as active and all the other lists objects to 
    public void StartPhase (int phaseNumber)
    {
        for (int i = 0; i < listOfPhaseData.Count; i++)
        {
            if (phaseNumber == i)
            {
                //Checks if each player is holding and object and makes them drop it
                if (blueCM.GetHoldingObject())
                {
                    Dictionary<string, object> interactParameters = blueCM.GetInteractParameters();
                    blueCM.GetHeldItem().GetComponent<iInteract>().Interact(interactParameters);
                }
                if (redCM.GetHoldingObject())
                {
                    Dictionary<string, object> interactParameters = redCM.GetInteractParameters();
                    redCM.GetHeldItem().GetComponent<iInteract>().Interact(interactParameters);
                }

                //Destroy all objects in scene that got moved out of their phase
                GameObject[] objects;
                foreach (string s in objectsToDestroy)
                {
                    objects = GameObject.FindGameObjectsWithTag(s);
                    foreach (GameObject g in objects)
                    {
                        Destroy(g);
                    }
                }

                foreach (GameObject g in listOfPhaseData[i].objectList)
                {
                    g.SetActive(true);
                }

                if (listOfPhaseData[i].phaseType == PhaseType.Attack)
                {
                    StartCoroutine(AttackPhaseTimer(listOfPhaseData[i].attackPhaseLength, i + 1));
                }

                //Sets spawns points on phase  change
                reloadSceneManager.SetPlayersRespawnBossFight();
                //tells tile data manager which map to use
                tilemapDataManager.SetActiveMap(phaseNumber+1);

                currentPhase = phaseNumber;
            }
            else
            {
                foreach(GameObject g in listOfPhaseData[i].objectList)
                {
                    g.SetActive(false);
                }
            }
        }

        CheckFinalPhase();
    }

    //set all objects in the list of the new phase as active and all the other lists objects to (overload for dev tool)
    public void StartPhase(InputAction.CallbackContext context)
    {
        StopAllCoroutines();
        int phaseNumber = currentPhase+1;
        for (int i = 0; i < listOfPhaseData.Count; i++)
        {
            if (phaseNumber == i)
            {
                //Checks if each player is holding and object and makes them drop it
                if (blueCM.GetHoldingObject())
                {
                    Dictionary<string, object> interactParameters = blueCM.GetInteractParameters();
                    blueCM.GetHeldItem().GetComponent<iInteract>().Interact(interactParameters);
                }
                if (redCM.GetHoldingObject())
                {
                    Dictionary<string, object> interactParameters = redCM.GetInteractParameters();
                    redCM.GetHeldItem().GetComponent<iInteract>().Interact(interactParameters);
                }

                //Destroy all objects in scene that got moved out of their phase
                GameObject[] objects;
                foreach (string s in objectsToDestroy)
                {
                    objects = GameObject.FindGameObjectsWithTag(s);
                    foreach (GameObject g in objects)
                    {
                        Destroy(g);
                    }
                }

                foreach (GameObject g in listOfPhaseData[i].objectList)
                {
                    g.SetActive(true);
                }

                if (listOfPhaseData[i].phaseType == PhaseType.Attack)
                {
                    StartCoroutine(AttackPhaseTimer(listOfPhaseData[i].attackPhaseLength, i + 1));
                }

                //Sets spawns points on phase  change
                reloadSceneManager.SetPlayersRespawnBossFight();
                //tells tile data manager which map to use
                tilemapDataManager.SetActiveMap(phaseNumber + 1);

                currentPhase = phaseNumber;
            }
            else
            {
                foreach (GameObject g in listOfPhaseData[i].objectList)
                {
                    g.SetActive(false);
                }
            }
        }

        CheckFinalPhase();
    }

    private IEnumerator AttackPhaseTimer(float phaseLength, int nextPhase)
    {
        yield return new WaitForSeconds(phaseLength);

        StartPhase(nextPhase);
    }

    //On final phase tells boss object to start moving etc.
    public void CheckFinalPhase()
    {
        if (currentPhase == listOfPhaseData.Count-1)
        {
            if (boss.TryGetComponent<BossBehaviour>(out var bb))
            {
                bb.SetActivateBoss(true);
            }
        }
    }

    //Sets controls for dev tool
    private void SetControls()
    {
        playerControls = GameObject.FindGameObjectWithTag("InputControllerManager").GetComponent<PlayerInputController>().GetBlueCharacterControls();
        playerControls.PlayerBlue.SkipPhase.performed += StartPhase;
    }

    //Destroys reference to controls on destroy
    private void OnDestroy()
    {
        playerControls.PlayerBlue.SkipPhase.performed -= StartPhase;
    }
}
