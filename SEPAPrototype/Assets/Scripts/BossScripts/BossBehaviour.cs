using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

//Originally written by Tim Harrison on 23/9/22
//Edited by Tim Harrison on 26/9/22 - Added explosions to boss death
//Edited by Tim Harrison on 25/10/22 - Added bool that when true tells the boss to move and flash
//Edited by Tim Harrison on 25/10/22 - updates sprite, plays screen shake and PS on hit
//Edited by Tim Harrison on 30/10/22 - plays sfx on hit and death

public class BossBehaviour : MonoBehaviour
{
    [SerializeField] private string nextScene;
    [SerializeField] private List<GameObject> explosions = new();
    [SerializeField] private float timeBetweenExplosions;
    [SerializeField] private float deathLength;
    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private Sprite injuredSprite;

    private ParticleSystem PS;
    private int numberOfLasersHitting;
    private int health = 2;
    private Animator animator;

    private Vector2 bossDestination;
    private bool activateBoss;
    private bool dead = false;

    private GameObject soundManagerObj;
    private SoundManager soundManager;

    // Start is called before the first frame update
    void Start()
    {
        soundManagerObj = GameObject.FindGameObjectWithTag("SoundManager");
        if (soundManagerObj != null)
        {
            soundManager = soundManagerObj.GetComponent<SoundManager>();
        }

        bossDestination = new Vector2(transform.position.x, transform.position.y - 1);
        activateBoss = false;
        PS = GetComponentInChildren<ParticleSystem>();
        PS.Stop();
        animator = GetComponent<Animator>();
        animator.enabled = false;
        numberOfLasersHitting = 0;
    }

    private void Update()
    {
        //if bool, then boss moves and flashes
        if (activateBoss)
        {
            transform.position = Vector2.Lerp(transform.position, bossDestination, Time.deltaTime * 2);
            GetComponent<SpriteRenderer>().color = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time, 1));
        }

        //if lasers hitting boss activate injured sprite, screen shake and PS
        if (numberOfLasersHitting > 0)
        {
            animator.enabled = false;
            GetComponent<SpriteRenderer>().sprite = injuredSprite;
            FindObjectOfType<ScreenShake>().TriggerShake();
            PS.Play();
        }
        else
        {
            PS.Stop();
            GetComponent<SpriteRenderer>().sprite = defaultSprite;
            animator.enabled = true;
        }
    }

    //Increments damage when called
    public void incrementDamage()
    {
        PlaySound("bossScream");
        numberOfLasersHitting++;

        if (numberOfLasersHitting >= health)
        {
            StartCoroutine(PlayExplosions(timeBetweenExplosions));

            //If boss is dead plays explosion sfx
            if (!dead)
            {
                dead = true;
                StartCoroutine(PlayExplostionSFX(timeBetweenExplosions));
            }
        }
    }

    //Decrements damage when called
    public void decrementDamage()
    {
        if (numberOfLasersHitting > 0)
        {
            numberOfLasersHitting--;
        }
    }

    //Keeps playing explosion sfx till scene changes
    private IEnumerator PlayExplostionSFX(float duration)
    {
        foreach (GameObject g in explosions)
        {
            PlaySound("bossExplosion");
            yield return new WaitForSeconds(duration);
        }

        while (true)
        {
            PlaySound("bossExplosion");
            yield return new WaitForSeconds(0.3f);
        }
    }

    //Turns on the animator for each of the explosions
    private IEnumerator PlayExplosions(float duration)
    {        
        foreach (GameObject g in explosions)
        {
            g.SetActive(true);            
            yield return new WaitForSeconds(duration);
        }
        
        StartCoroutine(WaitForDeath(deathLength));
    }

    //Waits for given time before loading next scene
    private IEnumerator WaitForDeath(float duration)
    {
        yield return new WaitForSeconds(duration);

        SceneManager.LoadScene(nextScene);
    }

    //Method that handles all sound manager calls
    private void PlaySound(string sound)
    {
        if (soundManager != null)
        {
            soundManager.PlaySound(sound);
        }
    }

    public void SetActivateBoss(bool b)
    {
        activateBoss = b;
    }
}
