using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by Tim Harrison on - 29/8/22
//Edited by Tim Harrison on 9/9/22 - Now works when attached to projectile receivers

public class TriggerPhase : MonoBehaviour
{
    private enum Phases
    {
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
    }

    [SerializeField] private Phases PhaseToTrigger;

    private GameObject phaseManagerObj;
    private LevelPhases phaseManager;
    private string objectType;

    private ProjectileReceiver receiverScript;

    // Start is called before the first frame update
    void Start()
    {
        phaseManagerObj = GameObject.FindGameObjectWithTag("PhaseManager");
        if (phaseManagerObj != null)
        {
            phaseManager = phaseManagerObj.GetComponent<LevelPhases>();
        }

        objectType = gameObject.tag;

        //Gets receiver script on this object if it is a receiver
        if (objectType == "ProjectileReceiver")
        {
            receiverScript = GetComponent<ProjectileReceiver>();            
        }
    }

    private void Update()
    {
        //Increments phase when receiver is completed
        if (objectType == "ProjectileReceiver")
        {
            if (receiverScript.GetCompleted())
            {
                phaseManager.StartPhase((int)PhaseToTrigger);
            }
        }
    }

    //tells level manager to start new phase when a player walks into the trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (collision.transform.tag == "Player" && objectType != null)
        //{
        //    phaseManager.StartPhase((int)PhaseToTrigger);
        //}
    }
}
