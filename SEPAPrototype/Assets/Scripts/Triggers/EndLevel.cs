using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Edited by Nicholas Mueller on 6/5/22 - Updated script so that two players must be present for next scene to load
//Edited by Nicholas Mueller on 27/10/22 - Updated script to include a simple level transition between levels

public class EndLevel : MonoBehaviour
{
    [SerializeField] private string nextLevelSceneName;
    [SerializeField] private GameObject player1, player2;
    [SerializeField] private CanvasGroup lvlTransitionPanel;
    private bool player1End = false, player2End = false;
    private bool loadingLvl = false;

    //Sets player checks to true if they enter the trigger and if both are true then the next scene loads
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == player1.name)
        {
            player1End = true;
        } else if (collision.gameObject.name == player2.name)
        {
            player2End = true;
        }

        if (player1End && player2End && !loadingLvl)
        {
            loadingLvl = true;
            StartCoroutine(LevelTransition());
        }
    }

    //Sets player checks to false if they leave the trigger 
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == player1.name)
        {
            player1End = false;
        }
        else if (collision.gameObject.name == player2.name)
        {
            player2End = false;
        }
    }

    private IEnumerator LevelTransition()
    {
        lvlTransitionPanel.gameObject.SetActive(true);
        while (lvlTransitionPanel.alpha < 1)
        {
            lvlTransitionPanel.alpha += Time.deltaTime;
            yield return null;
        }
        SceneManager.LoadScene(nextLevelSceneName);
    }
}
